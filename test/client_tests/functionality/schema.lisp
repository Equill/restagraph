;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite
  functionality-schema
  :description "Installing/removing/listing schema-versions and installing subschemas."
  :in functionality)

(fiveam:in-suite functionality-schema)

(fiveam:test
  schema-basic
  "Basic checks of the schema API."
  ;; "any" resourcetype
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           ;; WTF? Breaks on a leading `/`
                           "resourcetypes/any"
                           :api "schema")
    (fiveam:is (= 200 status))
    (fiveam:is (equal "any" (gethash "name" body)))
    (fiveam:is (null (gethash "attributes" body)))
    (fiveam:is (null (gethash "dependent" body)))
    (fiveam:is (stringp (gethash "description" body)))
    (fiveam:is (listp (gethash "relationships" body)))
    (fiveam:is (= 1 (length (gethash "relationships" body))))
    (fiveam:is (every #'hash-table-p (gethash "relationships" body)))
    (fiveam:is (equal "SC_CREATOR" (gethash "name" (first (gethash "relationships" body)))))
    (fiveam:is (equal "any" (gethash "reltype" (first (gethash "relationships" body)))))
    (fiveam:is (equal "many:1" (gethash "cardinality" (first (gethash "relationships" body)))))
    (fiveam:is (listp (gethash "source-types" (first (gethash "relationships" body)))))
    (fiveam:is (= 1 (length (gethash "source-types" (first (gethash "relationships" body))))))
    (fiveam:is (equal "any" (first (gethash "source-types" (first (gethash "relationships" body)))))))
  ;; "SC_CREATOR" relationship
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           ;; WTF? Breaks on a leading `/`
                           "relationships/SC_CREATOR"
                           :api "schema")
    (fiveam:is (= 200 status))
    (fiveam:is (equal "SC_CREATOR" (gethash "name" body)))
    (fiveam:is (null (gethash "dependent" body)))
    (fiveam:is (equal "many:1" (gethash "cardinality" body)))
    (fiveam:is (equal "any" (gethash "reltype" body)))
    (fiveam:is (stringp (gethash "description" body)))
    (fiveam:is (listp (gethash "source-types" body)))
    (fiveam:is (= 1 (length (gethash "source-types" body))))
    (fiveam:is (equal "any" (first (gethash "source-types" body))))
    (fiveam:is (listp (gethash "target-types" body)))
    (fiveam:is (= 1 (length (gethash "target-types" body))))
    (fiveam:is (equal "People" (first (gethash "target-types" body))))))

(fiveam:test
  schema-atomic-upload-and-delete
  "Creating and deleting schema-versions, and uploading subschemas."
  :depends-on 'schema-basic
  ;; Check the existing list of versions
  (multiple-value-bind (status initial-versions)
    (syscat-client:get-request *server* "?version=list" :api "schema")
    (fiveam:is (= 200 status))
    (fiveam:is (listp (gethash "versions" initial-versions)))
    (fiveam:is (integerp (gethash "current-version" initial-versions)))
    ;; Install a new subschema
    (fiveam:is (= 201 (install-schema
                        *server*
                        (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                                       :name "test_schema.json"))))
    ;; Confirm that the list of versions has gained a newer one, and that the new one is current
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "?version=list" :api "schema")
      (fiveam:is (= 200 status))
      (fiveam:is (> (length (gethash "versions" body))
                    (length (gethash "versions" initial-versions))))
      (fiveam:is (> (gethash "current-version" body)
                    (gethash "current-version" initial-versions))))
    ;; Fetch the schema, and check it for an expected value
    (let ((rschema (nth-value 1 (syscat-client:get-request *server*
                                                       "resourcetypes/Buildings"
                                                       :api "schema"))))
      (fiveam:is (null (gethash "dependent" rschema))))
    ;; Delete the schema-version we created
    (fiveam:is (= 200 (remove-last-schema *server*)))))

(fiveam:test
  schema-stepwise-upload-and-delete
  "Create a new schema-version and upload a subschema, but in discrete steps."
  :depends-on 'schema-atomic-upload-and-delete
  ;; Get the existing list of versions
  (let ((existing-versions
          (nth-value 1 (syscat-client:get-request *server* "?version=list" :api "schema"))))
    ;; Create a new schema-version, without adding anything to the core schema
    (log:debug "Create new schema version")
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/"
                                              :api "schema"
                                              :payload '(("create" . "true")))))
    ;; Confirm that the list of versions has gained a newer one, and that the new one is current
    (log:debug "Confirm that we have a new schema version")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "?version=list" :api "schema")
      (fiveam:is (= 200 status))
      (fiveam:is (> (length (gethash "versions" body))
                    (length (gethash "versions" existing-versions))))
      (fiveam:is (> (gethash "current-version" body)
                    (gethash "current-version" existing-versions)))))
  ;; Confirm that some element of the test schema is *not* present
  (log:debug "Verify that some new element is not present")
  (fiveam:is (= 404 (syscat-client:get-request *server*
                                           "resourcetypes/Buildings"
                                           :api "schema"
                                           :expected-status-code 404)))
  ;; Take a new snapshot of the schema versions.
  ;; We can calculate the new number of versions by incrementing the previous value by one,
  ;; but the *value* of the current version is a snapshot.
  ;; That's harder to predict, so I'm just grabbing a whole new copy to make sure.
  (let ((existing-versions
          (nth-value 1 (syscat-client:get-request *server* "?version=list" :api "schema"))))
    ;; Upload the test schema
    (log:debug "Upload the test schema")
    (fiveam:is (= 201 (syscat-client:post-request
                        *server*
                        "/"
                        :api "schema"
                        :payload `(("schema"
                                    . ,(make-pathname :defaults (format nil "~A/functionality/"
                                                                        *test-directory*)
                                                      :name "test_schema.json"))))))
    ;; Confirm that the schema versioning has *not* changed as a result
    (log:debug "Confirm that we don't have a new schema version")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "?version=list" :api "schema")
      (fiveam:is (= 200 status))
      (fiveam:is (= (length (gethash "versions" body))
                    (length (gethash "versions" existing-versions))))
      (fiveam:is (= (gethash "current-version" body)
                    (gethash "current-version" existing-versions)))))
  ;; Check that the new schema is present
  (log:debug "Check for a new schema element that should now be there")
  (let ((rschema (nth-value 1 (syscat-client:get-request *server*
                                                     "resourcetypes/Buildings"
                                                     :api "schema"))))
    (fiveam:is (null (gethash "dependent" rschema))))
  ;; Delete the version we created
  (log:debug "Delete this test schema-version")
  (fiveam:is (= 200 (remove-last-schema *server*))))

(fiveam:test
  schema-parsing
  "Confirm that invalid definitions are gracefully ignored by the server."
  :depends-on 'schema-basic
  ;; Upload the test schema
  (fiveam:is (= 201
                (install-schema *server* (make-pathname :defaults (format nil "~A/functionality/"
                                                                          *test-directory*)
                                                        :name "parsing_test_schema.json"))))
  ;; Confirm that the misnamed attribute is not present
  (let ((rtype (nth-value 1 (syscat-client:get-request *server*
                                                   "resourcetypes/Buildings"
                                                   :api "schema"))))
    (fiveam:is (= 1 (length (gethash "attributes" rtype)))))
  ;; Confirm that a relationship _was_ added to the expected type
  (let ((rtype (nth-value 1 (syscat-client:get-request *server*
                                                   "resourcetypes/Floors"
                                                   :api "schema"))))
    (fiveam:is (null (gethash "attributes" rtype)))
    (fiveam:is (= 1 (length (gethash "relationships" rtype)))))
  ;; Remove the schema we added
  (remove-last-schema *server*))
