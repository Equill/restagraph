;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;; It might seem odd that I check the status in every request.
;; It's the first-returned value from the syscat-client functions, so I need to write at least one line
;; to ignore it in m-v-b calls anyway.
;; If I'm writing that one line, it may as well be a test, just in case the server returns a status
;; code other than the one you expected under the circumstances.

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite functionality-relationships
                  :description "Connections between resources."
                  :in functionality)

(fiveam:in-suite functionality-relationships)

(fiveam:test
  relationships-basic
  "All the fundamental relationship stuff"
  :depends-on 'healthcheck
  (let ((person1 "Blake")
        (org1 "TheSeven")
        (validrel1 "MEMBER_OF_ORG")
        (invalidrel1 "Tagged")
        (scadmin "ScAdmin"))
    ;;; Connect a person to an organisation
    ;; Confirm neither is present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" person1))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/Organisations/~A" org1))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Create them
    (log:debug "TEST: Create the person and organisation")
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person1)))
    (syscat-client:post-request *server* "/Organisations" :payload `(("ScUID" . ,org1)))
    (log:debug "TEST: Connect the person to the organisation")
    ;; Create a relationship, but b0rk the target path
    (fiveam:is (= 400
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A" person1)
                    :payload `(("target" . ,(format nil "/Organiz00tions/~A" org1))))))
    ;; Try to create a relationship, but forget to mention what kind
    (fiveam:is (= 400
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A" person1)
                    :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Delete the resources
    ;; Connect them
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A/~A" person1 validrel1)
                    :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Verify the relationship
    (log:debug "TEST: Verify the relationship")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/People/~A/~A/Organisations" person1 validrel1))
      (fiveam:is (= 200 status))
      (fiveam:is (equal org1 (gethash "ScUID" (car body)))))
    ;; Fail to delete the resource at the end of the relationship,
    ;; because this path is not canonical
    (log:debug "TEST: Fail to delete the relationship with a non-canonical path")
    (fiveam:is (= 400 (syscat-client:delete-request
                        *server*
                        (format nil "/People/~A/~A/Organisations/~A" person1 validrel1 org1))))
    ;; Delete the relationship
    (log:debug "TEST: Successfully delete the relationship")
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        (format nil "/People/~A/~A" person1 validrel1)
                        :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Confirm the relationship is gone
    (log:debug "TEST: Confirm the relationship is gone")
    (multiple-value-bind (status body)
      (syscat-client:get-request
        *server*
        (format nil "/People/~A/~A/Organisations" person1 validrel1))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Re-create that relationship, but with 1 spurious element prepended to the target path.
    ;; The use-case we're covering here is client-side copypasta, where the user has accidentally
    ;; copied a little too much of the path from the location bar. Or misbehaving automation.
    (log:debug "TEST: Create relationship with 1 spurious element prepended to target path")
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A/~A" person1 validrel1)
                    :payload `(("target" . ,(format nil "y/Organisations/~A" org1))))))
    (log:debug "TEST: Verify the relationship")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/People/~A/~A/Organisations" person1 validrel1))
      (fiveam:is (= 200 status))
      (fiveam:is (equal org1 (gethash "ScUID" (car body)))))
    (log:debug "TEST: Successfully delete the relationship")
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        (format nil "/People/~A/~A" person1 validrel1)
                        :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Same thing again, but with 2 spurious elements.
    (log:debug "TEST: Create relationship with 1 spurious element prepended to target path")
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A/~A" person1 validrel1)
                    :payload `(("target" . ,(format nil "x/y/Organisations/~A" org1))))))
    (log:debug "TEST: Verify the relationship")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/People/~A/~A/Organisations" person1 validrel1))
      (fiveam:is (= 200 status))
      (fiveam:is (equal org1 (gethash "ScUID" (car body)))))
    (log:debug "TEST: Successfully delete the relationship")
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        (format nil "/People/~A/~A" person1 validrel1)
                        :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Same thing again, but with 2 spurious elements and a leading forward-slash.
    (log:debug "TEST: Create relationship with 1 spurious element prepended to target path")
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "/People/~A/~A" person1 validrel1)
                    :payload `(("target" . ,(format nil "/x/y/Organisations/~A" org1))))))
    (log:debug "TEST: Verify the relationship")
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/People/~A/~A/Organisations" person1 validrel1))
      (fiveam:is (= 200 status))
      (fiveam:is (equal org1 (gethash "ScUID" (car body)))))
    (log:debug "TEST: Successfully delete the relationship")
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        (format nil "/People/~A/~A" person1 validrel1)
                        :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Fail to create an invalid relationship
    ;; Now we're handling the case where the user mis-transcribed, or mistakenly assumed the target
    ;; is already present.
    (log:debug "TEST: Fail to create an invalid relationship")
    (multiple-value-bind (status message)
      (syscat-client:post-request
                 *server*
                 (format nil "/People/~A/~A" person1 invalidrel1)
                 :payload `(("target" . ,(format nil "/Organisations/~A" org1))))
      (declare (ignore message))
      (fiveam:is (= 409 status)))
    ;; Fail to delete invalid relationship
    (log:debug "TEST: Fail to delete an invalid relationship")
    (multiple-value-bind (status message)
      (syscat-client:delete-request
                 *server*
                 (format nil "/People/~A/~A" person1 invalidrel1)
                 :payload `(("target" . ,(format nil "/Organisations/~A" org1))))
      (declare (ignore message))
      (fiveam:is (= 400 status)))
    ;; Fail to set the creator of a resource
    (log:debug "TEST: Fail to set the creator of a resource")
    (multiple-value-bind (status message)
      (syscat-client:post-request
                 *server*
                 (format nil "/People/~A/SC_CREATOR" person1)
                 :payload `(("target" . ,(format nil "/People/~A" scadmin))))
      (declare (ignore message))
      (fiveam:is (= 403 status)))
    ;; Fail to remove the creator relationship for a resource
    (log:debug "TEST: Fail to remove the creator relationship for a resource")
    (multiple-value-bind (status message)
      (syscat-client:delete-request
                 *server*
                 (format nil "/People/~A/SC_CREATOR" person1)
                 :payload `(("target" . ,(format nil "/People/~A" scadmin))))
      (declare (ignore message))
      (fiveam:is (= 403 status)));; Remove both test-resources
    (log:debug "TEST: Clean up the test resources")
    (syscat-client:delete-request *server* (format nil "/People/~A" person1))
    (syscat-client:delete-request *server* (format nil "/Organisations/~A" org1))))

(fiveam:test
  same-rel-to-dependent-of-existing-target
  "Check whether we can correctly create a relationship to a dependent subresource of a resource
  to which we already have that same relationship."
  :depends-on 'relationships-basic
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (parent-type "Buildings")
        (parent-uid "Headquarters")
        (dependent-rel "CONTAINS_ROOMS")
        (child-type "Rooms")
        (child-uid "CornerOffice")
        (source-type "People")
        (source-uid "CEO")
        (duplicate-rel "TENANT_IN"))
    ;; Install the test schema
    (install-schema *server* schema-path)
    ;; Ensure the target resources aren't already present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" parent-type parent-uid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Add the parent resource
    (syscat-client:post-request *server*
                            (format nil "/~A" parent-type)
                            :payload `(("ScUID" . ,parent-uid)))
    ;; Add the child
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list parent-type parent-uid dependent-rel child-type))
                            :payload `(("ScUID" . ,child-uid)))
    ;; Confirm the child is present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list parent-type
                                                         parent-uid
                                                         dependent-rel
                                                         child-type
                                                         child-uid)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal child-uid (gethash "ScUID" (first body)))))
    ;; Create the source resource
    (syscat-client:post-request *server*
                            (format nil "/~A" source-type)
                            :payload `(("ScUID" . ,source-uid)))
    ;; Connect the source to the target parent
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list source-type source-uid duplicate-rel))
                            :payload `(("target" . ,(format nil "~{/~A~}"
                                                            (list parent-type parent-uid)))))
    ;; Confirm that relationship is valid
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list source-type source-uid duplicate-rel parent-type)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal parent-uid (gethash "ScUID" (first body)))))
    ;; Connect the source to the target child
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list source-type
                                                        source-uid
                                                        duplicate-rel))
                            :payload `(("target" . ,(format nil "~{/~A~}"
                                                            (list parent-type
                                                                  parent-uid
                                                                  dependent-rel
                                                                  child-type
                                                                  child-uid)))))
    ;; Confirm that the source now has that relationship to two resources
    (multiple-value-bind (status body)
      (syscat-client:get-request
        *server*
        (format nil "~{/~A~}" (list source-type source-uid duplicate-rel)))
      (fiveam:is (= 200 status))
      (fiveam:is (= 2 (length body))))
    ;; Confirm that the relationship to the child is valid
    (multiple-value-bind (status body)
      (syscat-client:get-request
        *server*
        (format nil "~{/~A~}" (list source-type
                                    source-uid
                                    duplicate-rel
                                    child-type)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal child-uid (gethash "ScUID" (first body)))))
    ;; Reconfirm that the relationship to the parent is still present
    (multiple-value-bind (status body)
      (syscat-client:get-request
        *server*
        (format nil "~{/~A~}" (list source-type
                                    source-uid
                                    duplicate-rel
                                    parent-type)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal parent-uid (gethash "ScUID" (first body)))))
    ;; Delete the source resource
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" source-type source-uid)
                              :payload '(("recursive" . "true")))
    ;; Delete the parent, recursively
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" parent-type parent-uid)
                              :payload '(("recursive" . "true")))
    ;; Ensure the target resources are gone
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" parent-type parent-uid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Remove the test schema
    (remove-last-schema *server*)))

(fiveam:test
  relationship-multiples
  "1:many relationship"
  :depends-on 'same-rel-to-dependent-of-existing-target
  (let ((schema-path
          (make-pathname
            :defaults
            (format nil "~A/devel/syscat/syscat/test/client_tests/functionality/test_schema.json"
                    (user-homedir-pathname))))
        ;; Source resource
        (source-type "People")
        (source-uid "Dorian")
        (relationship "TENANT_IN")
        ;; Target common leaf elements
        (target-parent-type "Buildings")
        (target-rel "CONTAINS_ROOMS")
        (target-child-type "Rooms")
        (target-child-uid "Control_room")
        ;; Target 1
        (target-1-parent-uid "Xenon")
        ;; Target 2
        (target-2-parent-uid "Scorpio"))
    ;; Install the test schema
    (log:debug "Installing test schema")
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    "/"
                    :api "schema"
                    :payload `(("schema" . (,schema-path :content-type "application/json"))
                               ("create" . "true")))))
    ;; Ensure the test resources aren't already present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" source-type source-uid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" target-parent-type target-1-parent-uid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" target-parent-type target-2-parent-uid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Add the test resources
    (log:debug "Add test resources")
    (syscat-client:post-request *server*
                            (format nil "/~A" source-type)
                            :payload `(("ScUID" . ,source-uid)))
    (syscat-client:post-request *server*
                            (format nil "/~A" target-parent-type)
                            :payload `(("ScUID" . ,target-1-parent-uid)))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list target-parent-type
                                                        target-1-parent-uid
                                                        target-rel
                                                        target-child-type))
                            :payload `(("ScUID" . ,target-child-uid)))
    (syscat-client:post-request *server*
                            (format nil "/~A" target-parent-type)
                            :payload `(("ScUID" . ,target-2-parent-uid)))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list target-parent-type
                                                        target-2-parent-uid
                                                        target-rel
                                                        target-child-type))
                            :payload `(("ScUID" . ,target-child-uid)))
    ;; Create the first relationship
    (log:debug "Create first relationship")
    (fiveam:is (equal 201
                      (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list source-type
                                                    source-uid
                                                    relationship))
                        :payload `(("target" . ,(format nil "~{/~A~}" (list target-parent-type
                                                                            target-1-parent-uid
                                                                            target-rel
                                                                            target-child-type
                                                                            target-child-uid)))))))
    ;; Confirm that relationship is there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list source-type
                                                         source-uid
                                                         relationship
                                                         target-child-type)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (when body
                   (equal target-child-uid
                          (gethash "ScUID" (first body))))))
    ;; Create the second relationship
    (fiveam:is (equal 201
                      (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list source-type
                                                    source-uid
                                                    relationship))
                        :payload `(("target" . ,(format nil "~{/~A~}" (list target-parent-type
                                                                            target-2-parent-uid
                                                                            target-rel
                                                                            target-child-type
                                                                            target-child-uid)))))))
    ;; Confirm it's there, too.
    ;; Confirm *both* relationships are there.
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list source-type
                                                         source-uid
                                                         relationship
                                                         target-child-type)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (= 2 (length body)))
      (fiveam:is (when body
                   (equal target-child-uid
                          (gethash "ScUID" (first body))))))
    ;; Remove the test resources
    (log:debug "Remove test resources")
    (syscat-client:delete-request *server* (format nil "/~A/~A" source-type source-uid))
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" target-parent-type target-1-parent-uid)
                              :payload `(("recursive" . "true")))
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" target-parent-type target-2-parent-uid)
                              :payload `(("recursive" . "true")))))

(fiveam:test
  relationship-sanitisation
  "Sanitising things like trailing spaces"
  :depends-on 'relationships-basic
  (let ((person1 "Dayna")
        (rel1 "MEMBER_OF_ORG")
        (org1 "ScorpioCrew")
        (org1-trailing-space "ScorpioCrew "))
    ;; Create the resources
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person1)))
    (syscat-client:post-request *server* "/Organisations" :payload `(("ScUID" . ,org1)))
    ;; Create the relationship with a trailing space, as though copy-pasta
    (multiple-value-bind (status message)
      (syscat-client:post-request
        *server*
        (format nil "/People/~A/~A" person1 rel1)
        :payload `(("target" . ,(format nil "/Organisations/~A" org1-trailing-space))))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/People/~A/~A/Organisations/~A"
                                person1 rel1 org1)
                        message)))
    ;; Confirm the relationship is there with the correct UID anyway
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/People/~A/~A/Organisations" person1 rel1))
    (fiveam:is (= 200 status))
    (fiveam:is (equal org1 (gethash "ScUID" (car body)))))
    ;; Delete the resources
    (syscat-client:delete-request *server* (format nil "/People/~A" person1))
    (syscat-client:delete-request *server* (format nil "/Organisations/~A" org1))))

(fiveam:test
  relationships-to-any
  "Server handling of relationships with 'any' on either or both ends."
  :depends-on 'relationship-failures
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (source-type "Thingy")
        (source-uid "Whatsit")
        (target-type "Organisations")
        (target-uid "fluffycorp")
        (relationship "DISCOMBOBULATES")
        ;(invalid-source-type "People")
        ;(invalid-source-uid "Wrongun")
        )
    ;; Add the test-schema
    (install-schema *server* schema-path)
    ;; Create the resources
    (syscat-client:post-request *server* (format nil "/~A" source-type) :payload `(("ScUID" . ,source-uid)))
    (syscat-client:post-request *server* (format nil "/~A" target-type) :payload `(("ScUID" . ,target-uid)))
    ;; Create relationships to/from "any"
    (fiveam:is (= 201 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A/~A" source-type source-uid relationship)
                        :payload `(("target" . ,(format nil "/~A/~A" target-type target-uid))))))
    ;; Verify the relationships
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/~A/~A/~A" source-type source-uid relationship))
      (fiveam:is (= 200 status))
      (fiveam:is (equal target-type
                        (gethash "ScType" (first body))))
      (fiveam:is (equal target-uid
                        (gethash "ScUID" (first body)))))
    ;; Delete the resources
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/~A/~A" source-type source-uid))))
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/~A/~A" target-type target-uid))))
    ;; Remove the test schema
    (remove-last-schema *server*)))
