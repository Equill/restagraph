;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite functionality-files
                  :description "File operations."
                  :in functionality)

(fiveam:in-suite functionality-files)

(fiveam:test
  files-api-basic
  "Upload, download, metadata and deletion of files."
  :depends-on 'relationships-basic
  (let* ((file1source "cats_cuddling.jpg")
         (file1name "Cuddling cats")
         (file1type "image_jpeg")
         (file1path (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                                   :name file1source)))
    ;; First, upload your file
    (log:debug "Uploading the file")
    (multiple-value-bind (status body)
      (syscat-client:upload-file *server*
                                 `(("name" . ,file1name)
                                   ("file" . ,file1path)))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/Files/~A" (sanitise-uid file1name)) body)))
    ;; Now check its metadata
    (log:debug "TEST: Checking the metadata")
    (let ((metadata (first
                      (nth-value 1
                                 (syscat-client:get-request *server*
                                                        (format nil "/Files/~A?SCattributes=title"
                                                                (sanitise-uid file1name)))))))
      (fiveam:is (equal file1name (gethash "title" metadata))))
    ;; Are the expected links present, between the file and creator?
    (fiveam:is (equal "ScAdmin"
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/Files/~A/SC_CREATOR/People"
                                                      (sanitise-uid file1name))))))))
    (fiveam:is (equal (sanitise-uid file1name)
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/People/ScAdmin/SC_UPLOADED/Files")))))))
    ;; Is it correctly linked to its MediaType/MIMEtype?
    (fiveam:is (equal file1type
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/Files/~A/HAS_MEDIA_TYPE/MediaTypes"
                                                      (sanitise-uid file1name))))))))
    ;; Can we follow the trail from MediaTypes to Files?
    (fiveam:is (equal (sanitise-uid file1name)
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/MediaTypes/~A/MEDIA_TYPE_OF/Files"
                                                      file1type)))))))
    ;; Integrity test: when we download the file, do we get back what we put in?
    (log:debug "TEST: Downloading the file")
    (let ((copy-path #P"/tmp/syscat-clientside-test-file"))
      ;; Download the file
      (with-open-file (tmpfile copy-path
                               :direction :output
                               :if-exists :supersede
                               :element-type '(unsigned-byte 8))
        (let ((body (nth-value 1 (syscat-client:get-request *server*
                                                        (format nil "/~A" (sanitise-uid file1name))
                                                        :api "files"))))
          (log:info (format nil "Length of body: ~D~%" (length body)))
          (log:info (format nil "Type of body: ~A~%" (type-of body)))
          (write-sequence body tmpfile)))
      ;; Compare its size with the source file
      (let ((stat-original (osicat-posix:stat file1path))
            (stat-copy (osicat-posix:stat copy-path)))
        (fiveam:is (= (osicat-posix:stat-size stat-original)
                      (osicat-posix:stat-size stat-copy)))
        ;; Compare digests
        (fiveam:is (equalp (ironclad:digest-file :sha3/256 file1path)
                           (ironclad:digest-file :sha3/256 copy-path))))
      ;; Delete the downloaded copy
      (delete-file copy-path))
    ;; Now remove the file from the server
    (log:debug "TEST: Deleting the file")
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/~A" (sanitise-uid file1name))
                                                :api "files")))
    ;; Ensure it's gone
    (log:debug "TEST: Confirm the file is gone")
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "/Files/~A" (sanitise-uid file1name))))))))

(fiveam:test
  files-api-duplicate-prevention
  "Confirm that we can't create duplicate files, by either name or content."
  :depends-on 'files-api-basic
  (let* ((file1path (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                                   :name "cats_cuddling.jpg"))
         (file1name1 "Cuddling cats")
         (file1name2 "Cute kitties")
         (file2path (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                                   :name "Alex_is_staaaarvinnngggg.jpg")))
    ;; Upload the first file
    (multiple-value-bind (status body)
      (syscat-client:upload-file *server*
                             `(("name" . ,file1name1)
                               ("file" . ,file1path)))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/Files/~A" (sanitise-uid file1name1)) body)))
    ;; Try re-uploading it by the same name
    (multiple-value-bind (status body)
      (syscat-client:upload-file *server*
                             `(("name" . ,file1name1)
                               ("file" . ,file1path)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal (format nil "/Files/~A" (sanitise-uid file1name1)) body)))
    ;; Try re-uploading it under a different name
    (multiple-value-bind (status body)
      (syscat-client:upload-file *server*
                             `(("name" . ,file1name2)
                               ("file" . ,file1path)))
      (fiveam:is (= 200 status))
      (fiveam:is (equal (format nil "/Files/~A" (sanitise-uid file1name1)) body)))
    ;; Try uploading a different file by the first one's name
    (multiple-value-bind (status body)
      (syscat-client:upload-file *server*
                             `(("name" . ,file1name1)
                               ("file" . ,file2path)))
      (declare (ignore body))
      (fiveam:is (= 500 status))
      ;(fiveam:is (equal (format nil "/Files/~A" (sanitise-uid file1name1)) body))
      )
    ;; Delete the file
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/~A"
                                                                 (sanitise-uid file1name1))
                                                :api "files")))
    ;; Confirm it's gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server*
                                                         (format nil "/Files/~A"
                                                                 (sanitise-uid file1name1))))))))
