;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite functionality-resources-primary
                  :description "Creating, modifying and deleting resources."
                  :in functionality)

(fiveam:in-suite functionality-resources-primary)

(fiveam:test
  healthcheck
  "Confirm that the healthcheck endpoint responds as expected."
  (fiveam:is (= 200 (syscat-client:get-request *server* "/healthcheck" :api "none"))))

(fiveam:test
  uid-validation
  "Check that UIDs are validated in all the ways, before being applied."
  :depends-on 'healthcheck
  (let ((lstripname " Barnabas")
        (lstripuid "Barnabas")
        (rstripname "Ezekiel ")
        (rstripuid "Ezekiel")
        (bstripname " Moloch ")
        (bstripuid "Moloch")
        (valid-uid1 "Del-Tarrant")
        (invalid-uid1 "Jenna$Stannis")
        (invalid-uid2 "Dayna'Mellanby"))
    ;;; Leading space
    ;; Ensure it isn't already there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" lstripuid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Create it
    (multiple-value-bind (status body)
      (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,lstripname)))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/People/~A" lstripuid) body)))
    ;; Confirm it's there
    (fiveam:is (= 200 (syscat-client:get-request *server* (format nil "/People/~A" lstripuid))))
    ;; Delete it
    (syscat-client:delete-request *server* (format nil "/People/~A" lstripuid))
    ;;;
    ;;; Trailing space
    ;; Ensure it isn't already there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" rstripuid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Create it
    (multiple-value-bind (status body)
      (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,rstripname)))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/People/~A" rstripuid) body)))
    ;; Confirm it's there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" rstripuid))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (typep (first body) 'hash-table)))
    ;; Delete it
    (syscat-client:delete-request *server* (format nil "/People/~A" rstripuid))
    ;;;
    ;;; Surrounding spaces
    ;; Ensure it isn't already there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" bstripuid))
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Create it
    (multiple-value-bind (status body)
      (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,bstripname)))
      (fiveam:is (= 201 status))
      (fiveam:is (equal (format nil "/People/~A" bstripuid) body)))
    ;; Confirm it's there
    (fiveam:is (= 200 (syscat-client:get-request *server* (format nil "/People/~A" bstripuid))))
    ;; Delete it
    (syscat-client:delete-request *server* (format nil "/People/~A" bstripuid))
    ;;
    ;; Valid/invalid UID characters
    ;; Valid #1
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,valid-uid1)))
    (fiveam:is (equal valid-uid1
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/People/~A" valid-uid1)))))))
    (syscat-client:delete-request *server* (format nil "/People/~A" valid-uid1))
    ;; Invalid #1
    (fiveam:is (= 201
                  (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,invalid-uid1)))))
    (fiveam:is (equal (sanitise-uid invalid-uid1)
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/People/~A" (sanitise-uid invalid-uid1))))))))
    (syscat-client:delete-request *server* (format nil "/People/~A" (sanitise-uid invalid-uid1)))
    ;; Invalid #2
    (fiveam:is (= 201
                  (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,invalid-uid2)))))
    (fiveam:is (equal (sanitise-uid invalid-uid2)
                      (gethash "ScUID"
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/People/~A" (sanitise-uid invalid-uid2))))))))
    (syscat-client:delete-request *server* (format nil "/People/~A" (sanitise-uid invalid-uid2)))))

(fiveam:test
  single-resource
  "Create and delete a single resource."
  :depends-on 'uid-validation
  (let ((restype "People")
        (resuid "Sam Spade")
        (invalidtype "interfaces")
        (invaliduid "eth0")
        (adminuser "ScAdmin"))
    (log:debug ";TEST Create and delete a single resource")
    ;; Confirm that it isn't already there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" restype (sanitise-uid resuid)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (null body)))
    ;; Create it
    (fiveam:is (= 201
                  (syscat-client:post-request *server*
                                          (format nil "/~A" restype)
                                          :payload `(("ScUID" . ,resuid)))))
    ;; Confirm it's now there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" restype (sanitise-uid resuid)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (equal (sanitise-uid resuid) (gethash "ScUID" (first body))))
      (fiveam:is (string= (format nil "/~A/~A" restype (sanitise-uid resuid))
                          (gethash "ScCanonicalpath" (first body)))))
    ;; Confirm we now have exactly 3 people, in two different ways
    ;; - Test via mod/3 == 1
    (fiveam:is (= 3
                  (length
                    (nth-value 1 (syscat-client:get-request *server*
                                                        (format nil "/~A" restype))))))
    ;; - Test via mod/3 == 0
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A/SC_CREATOR" restype (sanitise-uid resuid)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (= 1 (length body)))
      (fiveam:is (equal "People" (gethash "ScType" (first body))))
      (fiveam:is (equal adminuser (gethash "ScUID" (first body))))
      (fiveam:is (string= (format nil "/~A/~A" restype adminuser)
                          (gethash "ScCanonicalpath" (first body)))))
    ;; Fail to delete it
    (log:debug ";TEST Fail to delete the resource")
    (fiveam:is (= 400 (syscat-client:delete-request
                        *server*
                        (format nil "/~A/~A" invalidtype (sanitise-uid resuid)))))
    ;; Delete it
    (log:debug ";TEST Successfully delete the resource")
    (syscat-client:delete-request *server* (format nil "/~A/~A" restype (sanitise-uid resuid)))
    ;; Confirm it's gone
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" restype (sanitise-uid resuid)))
      (fiveam:is (= 200 status))
      (fiveam:is (listp body))
      (fiveam:is (null body)))
    (fiveam:is (= 2
                  (length
                    (nth-value 1 (syscat-client:get-request *server*
                                                        (format nil "/~A" restype))))))
    ;; Create it again, to test the "yoink" parameter to DELETE
    (fiveam:is (= 201
                  (syscat-client:post-request *server*
                                          (format nil "/~A" restype)
                                          :payload `(("ScUID" . ,resuid)))))
    ;; Delete it, with added yoink
    (let ((refcopy (nth-value 1 (syscat-client:get-request
                                  *server*
                                  (format nil "/~A/~A" restype (sanitise-uid resuid))))))
      (multiple-value-bind (status body)
        (syscat-client:delete-request *server*
                                  (format nil "/~A/~A" restype (sanitise-uid resuid))
                                  :payload '(("yoink" . "true")))
        (fiveam:is (= 200 status))
        (fiveam:is (equalp refcopy (shasht:read-json body)))))
    ;; Fail to create an resource of an invalid type
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "/~A/~A" invalidtype invaliduid))))))

(fiveam:test
  duplicate-resistance
  "Confirm that we can't create duplicate primary resources."
  :depends-on 'single-resource
  (let ((res1type "People")
        (res1uid "Dante"))
    ;; Create a new resource
    (syscat-client:post-request *server* (format nil "/~A" res1type) :payload `(("ScUID" . ,res1uid)))
    ;; Confirm its presence
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/~A/~A" res1type res1uid))
      (fiveam:is (= status 200))
      (equal res1uid (gethash "ScUID" (first body))))
    ;; Attempt to create a duplicate, and be rejected by the server
    (fiveam:is (= 200 (syscat-client:post-request *server*
                                              (format nil "/~A" res1type)
                                              :payload `(("ScUID" . ,res1uid)))))
    ;; Confirm there's still only one of these things
    (= 1
       (length (nth-value 1 (syscat-client:get-request
                              *server*
                              (format nil "/~A/~A" res1type res1uid)))))
    ;; Delete the resource
    (syscat-client:delete-request *server* (format nil "/~A/~A" res1type res1uid))))

(fiveam:test
  multiple-resources
  "Retrieve details of all resources of a given type."
  :depends-on 'single-resource
  (let ((res1uid "Dude")
        (res2uid "Fifi")
        (res3uid "Trixibelle"))
    ;; Confirm we're starting with only the default resources
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "/People")
      (fiveam:is (= 200 status))
      (fiveam:is (= 2 (length body)))
      (fiveam:is (equal "ScSystem" (gethash "ScUID" (first body))))
      (fiveam:is (equal "ScAdmin" (gethash "ScUID" (second body)))))
    ;; Add the first two resources
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/People"
                                              :payload `(("ScUID" . ,res1uid)))))
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/People"
                                              :payload `(("ScUID" . ,res2uid)))))
    ;; Verify that there are now four resources of this type
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "/People")
      (fiveam:is (= 200 status))
      (fiveam:is (= 4 (length body)))
      (let ((people (sort body #'string< :key (lambda (person) (gethash "ScUID" person)))))
        (fiveam:is (equal "ScAdmin" (gethash "ScUID" (third people))))
        (fiveam:is (equal "ScSystem" (gethash "ScUID" (fourth people))))))
    ;; Add the third resource
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/People"
                                              :payload `(("ScUID" . ,res3uid)))))
    ;; Verify that there are now five of them
    (fiveam:is (= 5 (length (nth-value 1 (syscat-client:get-request *server* "/People")))))
    ;; Delete the resources we added
    (syscat-client:delete-request *server* (format nil "/People/~A" res1uid))
    (syscat-client:delete-request *server* (format nil "/People/~A" res2uid))
    (syscat-client:delete-request *server* (format nil "/People/~A" res3uid))
    ;; Verify that we're back to only the default two resources
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "/People")
      (fiveam:is (= 200 status))
      (fiveam:is (= 2 (length body)))
      (fiveam:is (equal "ScSystem" (gethash "ScUID" (first body))))
      (fiveam:is (equal "ScAdmin" (gethash "ScUID" (second body)))))))

(fiveam:test
  basic-resource-errors
  "Confirm what happens when we make basic errors in resource-creation requests."
  :depends-on 'single-resource
  (let ((invalid-resourcetype "IjustMadeThisUp")
        (valid-resourcetype "People")
        (valid-uid1 "Soolin"))
    ;; Invalid resource-type
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "/~A" invalid-resourcetype)
                                              :payload `(("foo" . "bar")))))
    ;; Missing UID
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "/~A" valid-resourcetype)
                                              :payload `(("foo" . "bar")))))
    ;; Invalid non-UID parameters
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "/~A" valid-resourcetype)
                                              :payload `(("ScUID" . ,valid-uid1)
                                                         ("foo" . "bar")))))))

(fiveam:test
  attributes-basic
  "Add and modify resource attributes."
  :depends-on 'multiple-resources
  (let ((person1 "Blake")
        (attr1name "displayname")
        (attr1val "Roj Blake")
        (person2 "Vila"))
    ;;; Single attribute
    (log:debug "Add and remove a single attribute")
    ;; Create the resource
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person1)))
    ;; Check that it doesn't already have this attribute
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" person1))
      (fiveam:is (= 200 status))
      (fiveam:is (null (gethash attr1name (first body)))))
    ;; Add the attribute
    (fiveam:is (= 200 (syscat-client:post-request *server*
                                              (format nil "/People/~A" person1)
                                              :put-p t
                                              :payload `((,attr1name . ,attr1val)))))
    ;; Confirm that the attribute is now present and correct
    (multiple-value-bind (status body)
      ;; Remember we have to specifically request the attributes we need
      (syscat-client:get-request *server* (format nil "/People/~A?SCattributes=~A" person1 attr1name))
      (fiveam:is (= 200 status))
      (fiveam:is (equal attr1val (gethash attr1name (first body)))))
    ;; Delete the resource
    (syscat-client:delete-request *server* (format nil "/People/~A" person1))
    ;;;
    ;;; Non-addressable attribute
    ;;; Try to change an attribute that is not explicitly defined, and is only set on creation.
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person2)))
    (let ((current-date
            (gethash "ScCreateddate"
                     (first
                       (nth-value 1
                                  (syscat-client:get-request *server*
                                                         (format nil "/People/~A" person2)))))))
      (fiveam:is (= 400 (syscat-client:post-request *server*
                                                (format nil "/People/~A" person2)
                                                :put-p t
                                                :payload '(("ScCreateddate" . "12345")))))
      (fiveam:is (= current-date
                    (gethash "ScCreateddate"
                             (first
                               (nth-value 1
                                          (syscat-client:get-request *server*
                                                                 (format nil "/People/~A" person2))))))))
    (syscat-client:delete-request *server* (format nil "/People/~A" person2))
    ;;;
    ;;; Try to change an internally-managed attribute relating to versions of a resource
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person2)))
    ;; Get the current version ID
    (let ((version (gethash "ScVersion"
                            (first
                              (nth-value 1
                                         (syscat-client:get-request *server*
                                                                (format nil "/People/~A" person2)))))))
      ;; Try two different approaches to setting the version
      (fiveam:is (= 400 (syscat-client:post-request *server*
                                                (format nil "/People/~A?version=~D" person2 version)
                                                :put-p t
                                                :payload '(("ScVersion" . "12345")))))
      (fiveam:is (= 400 (syscat-client:post-request *server*
                                                (format nil "/People/~A" person2)
                                                :put-p t
                                                :payload '(("ScVersion" . "12345")))))
      ;; Confirm the version hasn't changed
      (fiveam:is (= version
                    (gethash "ScVersion"
                             (first
                               (nth-value 1
                                          (syscat-client:get-request *server*
                                                                 (format nil "/People/~A" person2))))))))
    ;; Delete the test resource again
    (syscat-client:delete-request *server* (format nil "/People/~A" person2))))

(fiveam:test
  attribute-values
  "Validation of attribute types"
  :depends-on 'attributes-basic
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "People")
        (res1uid "Dorian")
        (res1attr1name "real")
        (res1attr1val_valid "True")
        (res1attr1val_invalid "Banana")
        (res1attr2name "birthyear")
        (res1attr2val_valid 1970)
        (res1attr2val_invalid1 1898)
        (res1attr2val_invalid2 3898)
        (res1attr2val_invalid3 "wrong")
        (res2type "Buildings")
        (res2uid "ResidenceOne")
        (res2attr1name "type")
        (res2attr1val_valid "House")
        (res2attr1val_invalid1 "Apartment"))
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Create the resources
    (syscat-client:post-request *server*
                            (format nil "/~A" res1type)
                            :payload `(("ScUID" . ,res1uid)))
    (syscat-client:post-request *server*
                            (format nil "/~A" res2type)
                            :payload `(("ScUID" . ,res2uid)))
    ;;; Boolean attribute
    ;; Set a valid attribute
    (fiveam:is (= 200 (syscat-client:post-request *server*
                                              (format nil "/~A/~A" res1type res1uid)
                                              :put-p t
                                              :payload `((,res1attr1name . ,res1attr1val_valid)))))
    (fiveam:is (equal t
                      (gethash res1attr1name
                               (first
                                 (nth-value 1
                                            (syscat-client:get-request
                                              *server*
                                              (format nil "/~A/~A?SCattributes=~A"
                                                      res1type res1uid res1attr1name)))))))
    ;; Fail to set an invalid value
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res1type res1uid)
                        :put-p t
                        :payload `((,res1attr1name . ,res1attr1val_invalid)))))
    ;;;
    ;;; Integer attribute
    ;; Too low
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res1type res1uid)
                        :put-p t
                        :payload `((,res1attr2name . ,(format nil "~D" res1attr2val_invalid1))))))
    ;; Too high
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res1type res1uid)
                        :put-p t
                        :payload `((,res1attr2name . ,(format nil "~D" res1attr2val_invalid2))))))
    ;; Not a number
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res1type res1uid)
                        :put-p t
                        :payload `((,res1attr2name . ,(format nil "~D" res1attr2val_invalid3))))))
                        ;; Valid
    (fiveam:is (= 200 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res1type res1uid)
                        :put-p t
                        :payload `((,res1attr2name . ,(format nil "~D" res1attr2val_valid))))))
    ;;; Enum attribute
    ;; Not a member of the set
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res2type res2uid)
                        :put-p t
                        :payload `((,res2attr1name . ,(format nil "~D" res2attr1val_invalid1))))))
    ;; Valid value
    (fiveam:is (= 200 (syscat-client:post-request
                        *server*
                        (format nil "/~A/~A" res2type res2uid)
                        :put-p t
                        :payload `((,res2attr1name . ,(format nil "~D" res2attr1val_valid))))))
    ;; Remove the resources
    (syscat-client:delete-request *server* (format nil "/~A/~A" res1type res1uid))
    (syscat-client:delete-request *server* (format nil "/~A/~A" res2type res2uid))
    ;; Remove that schema
    (remove-last-schema *server*)))

(fiveam:test
  uid-change
  "Changing the UID of a resource, as distinct from any user-defined attributes."
  :depends-on '(attribute-values single-resource)
  (let ((uid1 "Docholli")
        (uid2 "Kemper")
        (uidduplicate1 "ScSystem"))
    ;; Create a person
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,uid1)))
    ;; Rename it to itself
    (fiveam:is (= 200
                  (syscat-client:post-request *server*
                                          (format nil "/People/~A" uid1)
                                          :put-p t
                                          :payload `(("ScUID" . ,uid1)))))
    ;; Fail to rename an invalid path-length
    (fiveam:is (= 400
                  (syscat-client:post-request *server*
                                          (format nil "/People/blah/~A" uid1)
                                          :put-p t
                                          :payload `(("ScUID" . ,uid1)))))
    ;; Fail to rename it to an existing user
    (fiveam:is (= 409
                  (syscat-client:post-request *server*
                                          (format nil "/People/~A" uid1)
                                          :put-p t
                                          :payload `(("ScUID" . ,uidduplicate1)))))
    ;; Successfully rename it
    (fiveam:is (= 200
                  (syscat-client:post-request *server*
                                          (format nil "/People/~A" uid1)
                                          :put-p t
                                          :payload `(("ScUID" . ,uid2)))))
    ;; Confirm the rename has taken effect
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" uid2))
      (fiveam:is (= 200 status))
      (fiveam:is (equal uid2 (gethash "ScUID" (first body))))
      (fiveam:is (string= (format nil "/People/~A" uid2)
                          (gethash "ScCanonicalpath" (first body)))))
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server* (format nil "/People/~A" uid1)))))
    ;; Delete it under its new name
    (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/People/~A" uid2))))
    ;; Ensure it's not there under either name
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server* (format nil "/People/~A" uid1)))))
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server* (format nil "/People/~A" uid2)))))))

(fiveam:test
  get-filters
  "Check the mechanism for filtering the results of GET requests"
  :depends-on 'multiple-resources
  (let ((org1 "foocorp")
        (org2 "barcorp")
        ;(person1 "ScAdmin")
        (person2 "Bruce"))
    ;; Create and link the resources
    (syscat-client:post-request *server* "/Organisations" :payload `(("ScUID" . ,org1)))
    (syscat-client:post-request *server* "/Organisations" :payload `(("ScUID" . ,org2)))
    (syscat-client:post-request *server* "/People" :payload `(("ScUID" . ,person2)))
    (fiveam:is
      (= 201
         (syscat-client:post-request *server*
                                 (format nil "/People/~A/MEMBER_OF_ORG" person2)
                                 :payload `(("target" . ,(format nil "/Organisations/~A" org1))))))
    ;; Confirm they're all present
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "/Organisations/~A" org1))))))
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "/Organisations/~A" org2))))))
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "/People/~A" person2))))))
    ;; Test the filtering
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             "/Organisations?SCinbound=/People/*/MEMBER_OF_ORG")
      (fiveam:is (= 200 status))
      (fiveam:is (= 1 (length body)))
      (fiveam:is (equal org1 (gethash "ScUID" (first body)))))
    ;; Delete the resources
    (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/Organisations/~A" org1))))
    (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/Organisations/~A" org2))))
    (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/People/~A" person2))))))

(fiveam:test
  resource-versions
  "Operations specifically relating to resource versioning"
  :depends-on 'single-resource
  (let ((res1type "Organisations")
        (res1uid "ESFA")
        (res1attr1name "description")
        (res1attr1val1 "Eastern Seaboard Fission Authority.")
        (res1versioncomment1 "Set the name.")
        ;(res1attr1val2 "Eastern Seaboard Fission Authority. You may remember them from Neuromancer.")
        ;(res1versioncomment2 "Clarify the name.")
        )
    ;; Verify that the resource isn't already present
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server*
                                                         (format nil "/~A/~A" res1type res1uid)))))
    ;; Create the resource
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "/~A" res1type)
                                              :payload `(("ScUID" . ,res1uid)))))
    ;; Capture its version status for later reference
    (let ((version-reference (nth-value 1 (syscat-client:get-request
                                            *server*
                                            (format nil "/~A/~A?ScVersion=list"
                                                    res1type res1uid)))))
      ;; Confirm that it has exactly one version
      (fiveam:is (= 1 (hash-table-count (gethash "versions" version-reference))))
      ;; Add a version
      (fiveam:is (= 200 (syscat-client:post-request *server*
                                                (format nil "/~A/~A" res1type res1uid)
                                                :put-p t
                                                :payload `((,res1attr1name . ,res1attr1val1)
                                                           ("ScVersioncomment" . ,res1versioncomment1)))))
      ;; Confirm that we now have two versions, and that the current version is newer than previously
      (let ((new-version-reference (nth-value 1 (syscat-client:get-request
                                                  *server*
                                                  (format nil "/~A/~A?ScVersion=list"
                                                          res1type res1uid)))))
        ;; Confirm that the comment was added
        (fiveam:is (= 2 (hash-table-count (gethash "versions" new-version-reference)))))
      ;; Set the version back to the previous one
      (fiveam:is (= 200 (syscat-client:post-request
                          *server*
                          (format nil "/~A/~A" res1type res1uid)
                          :put-p t
                          :payload `(("ScVersion" . ,(format nil "~D" (gethash "current-version" version-reference)))))))
      ;; Confirm that the current-version is back to the original one
      (fiveam:is (= (gethash "current-version" version-reference)
                    (gethash "current-version" (nth-value 1 (syscat-client:get-request
                                                              *server*
                                                              (format nil "/~A/~A?ScVersion=list"
                                                                      res1type res1uid))))))
      ;; Do it implicitly as well
      (fiveam:is (null (gethash res1attr1name
                                (first (nth-value 1 (syscat-client:get-request
                                                      *server*
                                                      (format nil "/~A/~A" res1type res1uid))))))))
    ;; Delete the resource
    (syscat-client:delete-request *server* (format nil "/~A/~A" res1type res1uid))))
