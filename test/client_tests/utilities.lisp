;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun install-schema (server schema-path &key cookie-jar)
  "Helper function to minimise the boilerplate involved in installing a schema.
  Always creates a new version."
  (syscat-client:post-request
    server
    "/"
    :api "schema"
    :payload `(("schema" . (,schema-path :content-type "application/json"))
               ("create" . "true"))
    :cookie-jar cookie-jar))

(defun remove-last-schema (server &key cookie-jar)
  "Minimise the boilerplate code involved in deleting the last-installed schema version"
  (syscat-client:delete-request
    server
    "/"
    :api "schema"
    :payload `(("version"
                . ,(format nil "~D"
                           (gethash "current-version"
                                    (nth-value 1 (syscat-client:get-request
                                                   server
                                                   "/?version=list"
                                                   :api "schema"
                                                   :cookie-jar cookie-jar))))))
    :cookie-jar cookie-jar))
