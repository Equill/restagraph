;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite main)
(fiveam:in-suite main)

;;;; Suite-wide configuration

(defparameter *server*
  (make-instance 'syscat-client:server
                 :hostname "syscat.onfire.onice"
                 :address "syscat.onfire.onice"
                 :port 4952))

(setf shasht:*read-default-null-value* nil)

;; Parent path for files used in this test-suite
(defparameter *test-directory*
  (make-pathname :directory
                 (format nil "~A/devel/syscat/syscat/test/client_tests"
                         (user-homedir-pathname))))


;;;; Common functions

(defun unreserved-char-p (c)
  "Test whether a character is unreserved, per section 2.3 of RFC 3986."
  (or (alphanumericp c)
      (char= #\- c)
      (char= #\. c)
      (char= #\_ c)
      (char= #\~ c)))

(defun sanitise-uid (uid)
  "Strip out UID-unfriendly characters, after replacing whitespace with underscores.
   UID-friendly means being an unreserved character as defined in RFC 3986."
  (declare (type (string) uid))
  (remove-if-not #'unreserved-char-p
                 (substitute #\_ #\Space (string-trim '(#\Space #\Tab #\Newline) uid))))
