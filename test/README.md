Test suites for Syscat
==========================

There are several of these, to cater for different situations.

The LDAP configs are the same for each, so they're in the `LDAP_setup` directory.

For the internal tests, loaded in the REPL as the `syscat-test` package, there's also `docker-compose.yml` in the `internal` subdirectory.


# General setup

To set up any given test suite:

1. Ensure you've built/downloaded the necesary docker images, especially the version of Syscat specified in `docker-compose.yml`.
2. Either ensure that the Docker gateway on your testing host is 192.0.2.1, or update the test configs to use the correct address.
3. Invoke the relevant docker stack
    - Note that the actual name of the stack isn't significant.
4. From the `LDAP_setup` subdirectory, run `setup_syscat-auth.bash` to create the test domain and users.
    - Note that you need to be in that directory for this to work, because it makes unqualified references to the LDIF files.
    - The output you should expect is a series of messages starting with "adding new entry" or "modifying entry", with no errors or other types of message.


# Internal tests

These test the CL functions from within; they're essentially the unit tests.

After running the general setup using the `docker-compose.yml` from the `internal_tests` subdirectory:

1. Start up the Docker containers: `docker stack deploy -c internal_tests/docker-compose.yml sctest`
2. Within a REPL session, evaluate the following two forms:
    1. `(asdf:load-system :syscat-test)`
    2. `(fiveam:run! 'syscat-test:main)`


# Client tests

These confirm Syscat's behaviour from the client end of the HTTP APIs, so are the integration tests.

There are several of these suites, to cover the various access-control permutations, and they're all configured to run against a Docker stack.

**Note**: The `ACCESS_POLICY` variable is passed as an environment variable. Between that and the use of multiple `docker-compose.yml` files to, well, compose things, it's just simpler to use the `start-docker.sh` script.


## How to run the test suites

Each subdirectory of `client_tests/` contains a `pytest` test-suite, a script for starting Docker containers in a suitable way, and any other resources required by that particular test like, say, a cute picture of a couple of cats snuggling together.

1. Create a virtualenv directory under the `client_tests` directory, activate it, and install the requirements:
    `virtualenv -p python39 venv; source venv/bin/activate; pip install -r requirements.txt`
2. `cd` into the relevant subdirectory.
3. Run `start-docker.sh` to spin up the relevant docker stack.
    - Wait until it's finished starting up. Running `docker logs -f <ID of Syscat container>` is usually reliable. Note that its output is buffered, so if you see a message about confirming the files upload directory, either it's good to go or you have a mysterious hang to investigate.
4. Run the tests, by simply invoking `pytest`.
    - If you want more detail, run `pytest -v`. You can get even more detail with `pytest -vv`, and more than you're likely to want with `pytest -vvv`.


## The test suites

A summary of what the suite in each subdirectory of `client_tests/` is for.


## functionality

This is the comprehensive test-suite for Syscat's functionality. The other suites are purely to confirm that the access-control policies work as intended.

This uses the "open" policy-set for authentication, meaning no authentication is required to perform any action.


## `Write_authenticated`

For the `write-authenticated` policy-set, except that python can't cope with hyphenated module names.

Verifies that a valid session is required to perform any destructive actions, but also that no specific group-membership is required.
