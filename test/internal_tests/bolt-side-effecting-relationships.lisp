;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for side-effecting code pertaining to relationships.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  bolt-side-effecting-relationships
  :description "Tests for side-effecting code pertaining to relationships."
  :in main)

(fiveam:in-suite bolt-side-effecting-relationships)

(fiveam:test
  relationships
  :depends-on 'resources-basic
  "Basic operations on relationships between resources"
  (let* ((to-type (syscat::make-incoming-rtypes :name "Asn"))
         (from-type (syscat::make-incoming-rtypes :name "Routers"))
         (relationship (syscat::make-incoming-rels
                         :NAME "ASN"
                         :SOURCE-TYPES (list (syscat::name from-type))
                         :CARDINALITY "many:many"
                         :TARGET-TYPES (list (syscat::name to-type))))
         (from-uid "bikini")
         (to-uid "64512")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Add the new resourcetypes and relationships
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "relationships"
        :resourcetypes (list from-type to-type)
        :relationships (list relationship))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Store the router
      (syscat::log-message :info ";TEST Creating the resources")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name from-type)
                                  `(("ScUID" . ,from-uid))
                                  *admin-user*)
      ;; Create the interface
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name to-type)
                                  `(("ScUID" . ,to-uid))
                                  *admin-user*)
      ;; Create a relationship between them
      (syscat::log-message :info (format nil ";TEST Create the relationship /~A/~A/~A/~A/~A"
                                             (syscat::name from-type)
                                             from-uid
                                             (syscat::name relationship)
                                             (syscat::name to-type)
                                             to-uid))
      (fiveam:is (null (syscat::create-relationship-by-path
                         session
                         (format nil "/~A/~A/~A"
                                 (syscat::name from-type)
                                 from-uid
                                 (syscat::name relationship))
                         (format nil "/~A/~A"
                                 (syscat::name to-type)
                                 to-uid)
                         resourcetype-schema)))
      ;; Confirm the relationship is there
      (syscat::log-message
        :info
        (format nil ";TEST Confirm the list of resources at the end of /~A/~A/~A"
                (syscat::name from-type) from-uid relationship))
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name from-type)
                                 from-uid
                                 (syscat::name relationship))))))
        (fiveam:is (equal (syscat::name to-type)
                          (gethash "ScType" result)))
        (fiveam:is (equal to-uid (gethash "ScUID" result))))
      ;; Delete the relationship
      (syscat::log-message :info (format nil ";TEST Delete the relationship from /~A/~A/~A to /~A/~A"
                                             (syscat::name from-type)
                                             from-uid
                                             (syscat::name relationship)
                                             (syscat::name to-type)
                                             to-uid))
      (fiveam:is (null (syscat::delete-relationship-by-path
                         session
                         resourcetype-schema
                         (format nil "/~A/~A/~A"
                                 (syscat::name from-type)
                                 from-uid
                                 (syscat::name relationship))
                         (format nil "/~A/~A" (syscat::name to-type) to-uid))))
      ;; Delete the router
      (syscat::log-message :info ";TEST Cleanup: removing the resources")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name from-type) from-uid)
                                           resourcetype-schema)
      ;; Delete the interface
      (syscat::delete-resource-by-path session
                                           (list (syscat::name to-type) to-uid)
                                           resourcetype-schema)
      ;; Delete the new schema-version
      (syscat::delete-schema-version session schema-version))))

(fiveam:test
  relationships-to-any
  :depends-on 'relationships
  "Confirm that we can create relationships with a defined target of 'any',
  but can't create 'just any' relationship."
  (let* ((source-type "Test")
         (newrel "FROBS")
         (source-uid "Whoomp")
         (target-type "Organisations")
         (target-uid "ThereItIs")
         ;(invalid-sourcetype "People")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Define a relationship with a target-resource of "any".
    ;; To keep things simple, create a new type altogether as its source-type.
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "relationships-to-any"
        :resourcetypes (list (syscat::make-incoming-rtypes :name source-type
                                                               :dependent nil))
        :relationships (list (syscat::make-incoming-rels
                               :name newrel
                               :source-types (list source-type)
                               :target-types (list target-type)
                               :cardinality "many:many"
                               :reltype "any"
                               :description "Test relationship to 'any'")))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create test instances
      (syscat::store-resource session
                                  resourcetype-schema
                                  source-type
                                  `(("ScUID" . ,source-uid))
                                  *admin-user*)
      (syscat::store-resource session
                                  resourcetype-schema
                                  target-type
                                  `(("ScUID" . ,target-uid))
                                  *admin-user*)
      ;; Create a relationship from the instance
      (fiveam:is (null (syscat::create-relationship-by-path
                         session
                         (format nil "/~A/~A/~A" source-type source-uid newrel)
                         (format nil "/~A/~A" target-type target-uid)
                         resourcetype-schema)))
      ;; Fail to create an invalid relationship from another resourcetype
      (fiveam:signals
        syscat::integrity-error
        (syscat::create-relationship-by-path
          session
          (format nil "/~A/~A/~A" source-type source-uid newrel)
          (format nil "/~A/~A" target-type target-uid)
          resourcetype-schema))
      ;; Delete the test instances
      (syscat::delete-resource-by-path session
                                           (list source-type source-uid)
                                           resourcetype-schema)
      (syscat::delete-resource-by-path session
                                           (list target-type target-uid)
                                           resourcetype-schema))
    ;; Delete the temporary schema-version
    (syscat::delete-schema-version session schema-version)
    ;; Clean up the Bolt session
    (neo4cl:disconnect session)))

(fiveam:test
  relationships-integrity
  :depends-on 'relationships
  "Basic operations on relationships between resources"
  (let* ((from-type (syscat::make-incoming-rtypes :name "Routers"))
         (to-type (syscat::make-incoming-rtypes :name "Asn"))
         (relationship
           (syscat::make-incoming-rels
             :NAME "ASN"
             :CARDINALITY "many:many"
             :SOURCE-TYPES (list (syscat::name from-type))
             :TARGET-TYPES (list (syscat::name to-type))))
         (from-uid "bikini")
         (to-uid "64512")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "relationships-integrity"
        :resourcetypes (list from-type to-type)
        :relationships (list relationship))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the resources
      (syscat::log-message :info ";TEST Creating the resources")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name from-type)
                                  `(("ScUID" . ,from-uid))
                                  *admin-user*)
      ;; Create the interface
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name to-type)
                                  `(("ScUID" . ,to-uid))
                                  *admin-user*)
      ;; Create a relationship between them
      (syscat::log-message :info ";TEST Create a relationship between them")
      (fiveam:is (null (syscat::create-relationship-by-path
                         session
                         (format nil "/~A/~A/~A"
                                 (syscat::name from-type)
                                 from-uid
                                 (syscat::name relationship))
                         (format nil "/~A/~A" (syscat::name to-type) to-uid)
                         resourcetype-schema)))
      ;; Confirm the relationship is there
      (syscat::log-message :info ";TEST Confirm that the relationship is there")
      (let ((result (car (syscat::get-resources session
                                                    resourcetype-schema
                                                    (list (syscat::name from-type)
                                                          from-uid
                                                          (syscat::name relationship))))))
        (syscat::log-message :debug (format nil "Received result ~A" result))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScType" result))))
        (fiveam:is (equal (syscat::name to-type)
                          (gethash "ScType" result)))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal to-uid
                          (gethash "ScUID" result))))
      ;; Confirm we get what we expect when checking what's at the end of the path
      (syscat::log-message :info ";TEST Confirm that we get what we expect at the end of the path.")
      (let ((result (car (syscat::get-resources session
                                                    resourcetype-schema
                                                    (list (syscat::name from-type)
                                                          from-uid
                                                          (syscat::name relationship))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScType" result))))
        (fiveam:is (equal (syscat::name to-type)
                          (gethash "ScType" result)))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal to-uid
                          (gethash "ScUID" result))))
      ;; Attempt to create a duplicate relationship between them
      (syscat::log-message :info ";TEST Attempt to create a duplicate relationship.")
      (fiveam:signals (syscat::integrity-error
                        (format nil "Relationship ~A already exists from ~A ~A to ~A ~A"
                                (syscat::name relationship)
                                (syscat::name from-type)
                                from-uid
                                (syscat::name to-type)
                                to-uid))
        (syscat::create-relationship-by-path
          session
          (format nil "/~A/~A/~A"
                  (syscat::name from-type)
                  from-uid
                  (syscat::name relationship))
          (format nil "/~A/~A" (syscat::name to-type) to-uid)
          resourcetype-schema))
      ;; Confirm we still only have one relationship between them
      (syscat::log-message :info ";TEST Confirm we still only have one relationship between them.")
      (let ((result (car (syscat::get-resources session
                                                    resourcetype-schema
                                                    (list (syscat::name from-type)
                                                          from-uid
                                                          (syscat::name relationship))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScType" result))))
        (fiveam:is (equal (syscat::name to-type)
                          (gethash "ScType" result)))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal to-uid
                          (gethash "ScUID" result))))
      ;; Delete the relationship
      (syscat::log-message :info ";TEST Delete the relationship.")
      (fiveam:is (null (syscat::delete-relationship-by-path
                         session
                         resourcetype-schema
                         (format nil "/~A/~A/~A/"
                                 (syscat::name from-type)
                                 from-uid
                                 (syscat::name relationship))
                         (format nil "/~A/~A/" (syscat::name to-type) to-uid))))
      ;; Clean-up: delete the resources
      (syscat::log-message :info ";TEST Cleaning up: removing the resources")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name from-type) from-uid)
                                           resourcetype-schema)
      (syscat::delete-resource-by-path session
                                           (list (syscat::name to-type) to-uid)
                                           resourcetype-schema))
    ;; Delete the temporary schema-version
    (syscat::delete-schema-version session schema-version)))

(fiveam:test
  relationship-types
  :depends-on 'relationships-integrity
  "Check enforcement of the various reltypes."
  (let* ((parent-type (syscat::make-incoming-rtypes :name "Computers"))
         (child-type (syscat::make-incoming-rtypes :name "NetworkInterfaces"
                                                       :dependent t))
         (grandchild-type "Ipv4Addresses")
         (pcrel (syscat::make-incoming-rels :name "HAS_IFACE"
                                                :reltype "dependent"
                                                :cardinality "1:many"
                                                :source-types (list (syscat::name parent-type))
                                                :target-types (list (syscat::name child-type))))
         (cgrel (syscat::make-incoming-rels :name "HAS_ADDR"
                                                :reltype "dependent"
                                                :cardinality "1:many"
                                                :source-types (list (syscat::name child-type))
                                                :target-types (list grandchild-type)))
         (p1uid "Comp1")
         (p2uid "Comp2")
         (c1uid "Eth0")
         (c2uid "Eth0")
         (c3uid "Eth1")
         (g1uid "192.168.1.1")
         (g2uid "192.168.1.2")
         (self-rel1 (syscat::make-incoming-rels
                      :name "HAS_MGMT_IFACE"
                      :reltype "self"
                      :cardinality "1:1"
                      :source-types (list (syscat::name parent-type))
                      :target-types (list grandchild-type)))
         (self-rel2 (syscat::make-incoming-rels
                      :name "MGMT_IFACE_FOR"
                      :reltype "self"
                      :cardinality "1:1"
                      :source-types (list grandchild-type)
                      :target-types (list (syscat::name parent-type))))
         (other-rel1 (syscat::make-incoming-rels
                       :name "CONNECTS_TO"
                       :reltype "other"
                       :cardinality "1:many" ; This is wrong in the real world, but useful in testing
                       :source-types (list (syscat::name child-type))
                       :target-types (list (syscat::name child-type))))
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationships
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "relationships-integrity"
        :resourcetypes (list parent-type child-type)
        :relationships (list pcrel cgrel self-rel1 self-rel2 other-rel1))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the parent and child resources
      (syscat::log-message :debug "Create the parent and child resources")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent-type)
                                  `(("ScUID" . ,p1uid))
                                  *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p1uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type))
                                            `(("ScUID" . ,c1uid))
                                            *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p1uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type))
                                            `(("ScUID" . ,c3uid))
                                            *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p1uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type)
                                                  c1uid
                                                  (syscat::name cgrel)
                                                  grandchild-type)
                                            `(("ScUID" . ,g1uid))
                                            *admin-user*)
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent-type)
                                  `(("ScUID" . ,p2uid))
                                  *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p2uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type))
                                            `(("ScUID" . ,c2uid))
                                            *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p2uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type)
                                                  c2uid
                                                  (syscat::name cgrel)
                                                  grandchild-type)
                                            `(("ScUID" . ,g2uid))
                                            *admin-user*)
      ;; Self relationship (positive): parent to grandchild
      (fiveam:is
        (null
          (syscat::create-relationship-by-path
            session
            (format nil "~{/~A~}" (list (syscat::name parent-type)
                                        p1uid
                                        (syscat::name self-rel1)))
            (format nil "~{/~A~}" (list (syscat::name parent-type)
                                        p1uid
                                        (syscat::name pcrel)
                                        (syscat::name child-type)
                                        c1uid
                                        (syscat::name cgrel)
                                        grandchild-type
                                        g1uid))
            resourcetype-schema)))
      ;; Verify that the self-relationship is present
      (fiveam:is (string= (format nil "~{/~A~}"
                                  (list (syscat::name parent-type)
                                        p1uid
                                        (syscat::name pcrel)
                                        (syscat::name child-type)
                                        c1uid
                                        (syscat::name cgrel)
                                        grandchild-type
                                        g1uid))
                          (gethash "ScCanonicalpath"
                                   (first
                                     (syscat::get-resources
                                       session
                                       resourcetype-schema
                                       (list (syscat::name parent-type)
                                             p1uid
                                             (syscat::name self-rel1)
                                             grandchild-type
                                             g1uid))))))
      ;; Self relationship (negative): parent to the grandchild of a different parent
      (fiveam:signals
        syscat::integrity-error
        (null
          (syscat::create-relationship-by-path
            session
            (format nil "~{/~A~}" (list (syscat::name parent-type)
                                        p1uid
                                        (syscat::name self-rel1)))
            (format nil "~{/~A~}" (list (syscat::name parent-type)
                                        p2uid
                                        (syscat::name pcrel)
                                        (syscat::name child-type)
                                        c2uid
                                        (syscat::name cgrel)
                                        grandchild-type
                                        g2uid))
            resourcetype-schema)))
      ;; There should still be one of these, from the succeeding test
      (fiveam:is
        (= 1 (length (syscat::get-resources
                       session
                       resourcetype-schema
                       (list (syscat::name parent-type)
                             p1uid
                             (syscat::name self-rel1)
                             grandchild-type
                             g1uid)))))
      ;; Self-rel mk2 (positive): grandchild to parent
      (fiveam:is
        (null
          (syscat::create-relationship-by-path session
                                                   (format nil "~{/~A~}"
                                                           (list (syscat::name parent-type)
                                                                 p1uid
                                                                 (syscat::name pcrel)
                                                                 (syscat::name child-type)
                                                                 c1uid
                                                                 (syscat::name cgrel)
                                                                 grandchild-type
                                                                 g1uid
                                                                 (syscat::name self-rel2)))
                                                   (format nil "~{/~A~}"
                                                           (list (syscat::name parent-type) p1uid))
                                                   resourcetype-schema)))
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name parent-type)
                            p1uid
                            (syscat::name pcrel)
                            (syscat::name child-type)
                            c1uid
                            (syscat::name cgrel)
                            grandchild-type
                            g1uid
                            (syscat::name self-rel2)
                            (syscat::name parent-type)))))
        (fiveam:is (= 1 (length result)))
        (fiveam:is
          (string= (format nil "/~A/~A" (syscat::name parent-type) p1uid)
                   (gethash "ScCanonicalpath"
                            (first result)))))
      ;; Self-rel mk2 (negative): grandchild to the wrong parent
      (fiveam:signals
        syscat::integrity-error
        (syscat::create-relationship-by-path session
                                                 (format nil "~{/~A~}"
                                                         (list (syscat::name parent-type)
                                                               p1uid
                                                               (syscat::name pcrel)
                                                               (syscat::name child-type)
                                                               c1uid
                                                               (syscat::name cgrel)
                                                               grandchild-type
                                                               g1uid
                                                               (syscat::name self-rel2)))
                                                 (format nil "~{/~A~}"
                                                         (list (syscat::name parent-type) p2uid))
                                                 resourcetype-schema))
      (fiveam:is
        (= 1 (length
               (syscat::get-resources
                 session
                 resourcetype-schema
                 (list (syscat::name parent-type)
                       p1uid
                       (syscat::name pcrel)
                       (syscat::name child-type)
                       c1uid
                       (syscat::name cgrel)
                       grandchild-type
                       g1uid
                       (syscat::name self-rel2)
                       (syscat::name parent-type))))))
      ;; "Other" relationship: positive
      (syscat::log-message :debug "Successfully create 'other' relationship")
      (fiveam:is (null (syscat::create-relationship-by-path
                         session
                         (format nil "~{/~A~}" (list (syscat::name parent-type)
                                                     p1uid
                                                     (syscat::name pcrel)
                                                     (syscat::name child-type)
                                                     c1uid
                                                     (syscat::name other-rel1)))
                         (format nil "~{/~A~}" (list (syscat::name parent-type)
                                                     p2uid
                                                     (syscat::name pcrel)
                                                     (syscat::name child-type)
                                                     c2uid))
                         resourcetype-schema)))
      (syscat::log-message :debug "Verify presence of 'other' relationship")
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name parent-type)
                            p1uid
                            (syscat::name pcrel)
                            (syscat::name child-type)
                            c1uid
                            (syscat::name other-rel1)
                            (syscat::name child-type)))))
        (fiveam:is
          (string= (format nil "~{/~A~}" (list (syscat::name parent-type)
                                               p2uid
                                               (syscat::name pcrel)
                                               (syscat::name child-type)
                                               c2uid))
                   (gethash "ScCanonicalpath" (first result)))))
      ;; "Other" relationship: negative
      (syscat::log-message :debug "Fail to create 'other' relationship to self")
      (fiveam:signals
        syscat::integrity-error
        (syscat::create-relationship-by-path
                         session
                         (format nil "~{/~A~}" (list (syscat::name parent-type)
                                                     p1uid
                                                     (syscat::name pcrel)
                                                     (syscat::name child-type)
                                                     c1uid
                                                     (syscat::name other-rel1)))
                         (format nil "~{/~A~}" (list (syscat::name parent-type)
                                                     p1uid
                                                     (syscat::name pcrel)
                                                     (syscat::name child-type)
                                                     c3uid))
                         resourcetype-schema))
      (syscat::log-message :debug "Verify absence of 'other' relationship")
      (fiveam:is (= 1 (length (syscat::get-resources
                       session
                       resourcetype-schema
                       (list (syscat::name parent-type)
                             p1uid
                             (syscat::name pcrel)
                             (syscat::name child-type)
                             c1uid
                             (syscat::name other-rel1)
                             (syscat::name child-type))))))
      ;; Clean-up: delete the resources
      (syscat::log-message :info ";TEST Cleaning up: removing the resources")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name parent-type) p1uid)
                                           resourcetype-schema
                                           :recursive t)
      (syscat::delete-resource-by-path session
                                           (list (syscat::name parent-type) p2uid)
                                           resourcetype-schema
                                           :recursive t))
    ;; Delete the temporary schema-version
    (syscat::delete-schema-version session schema-version)))

(fiveam:test
  regression-tests
  :depends-on 'relationships-integrity
  "Past problems that might or might not still be fixed."
  (let* ((parent-type (syscat::make-incoming-rtypes :name "Brands"))
         (child-type (syscat::make-incoming-rtypes :name "Models"
                                                       :dependent t))
         (grandchild-type (syscat::make-incoming-rtypes :name "ModelVariations"
                                                            :dependent t))
         (pcrel (syscat::make-incoming-rels :name "MANUFACTURES"
                                                :reltype "dependent"
                                                :cardinality "1:many"
                                                :source-types (list (syscat::name parent-type))
                                                :target-types (list (syscat::name child-type))))
         (cprel (syscat::make-incoming-rels :name "MANUFACTURED_BY"
                                                :reltype "any"
                                                :cardinality "many:1"
                                                :source-types (list (syscat::name child-type))
                                                :target-types (list (syscat::name parent-type))))
         (cgrel (syscat::make-incoming-rels :name "HAS_VARIANT"
                                                :reltype "dependent"
                                                :cardinality "1:many"
                                                :source-types (list (syscat::name child-type))
                                                :target-types (list (syscat::name grandchild-type))))
         (gcrel (syscat::make-incoming-rels :name "VARIANT_OF"
                                                :reltype "any"
                                                :cardinality "many:1"
                                                :source-types (list (syscat::name grandchild-type))
                                                :target-types (list (syscat::name child-type))))
         (p1uid "Diamine")
         (c1uid "FountainPenInks")
         (g1uid "SapphireBlue")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "relationships-integrity"
        :resourcetypes (list parent-type child-type grandchild-type)
        :relationships (list pcrel cprel cgrel gcrel))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Create the parent and child resources, including the back-link
      (syscat::log-message :debug "Create the parent and child resources, including the back-link")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent-type)
                                  `(("ScUID" . ,p1uid))
                                  *admin-user*)
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p1uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type))
                                            `(("ScUID" . ,c1uid))
                                            *admin-user*)
      (syscat::create-relationship-by-path session
                                               (format nil "~{/~A~}"
                                                       (list (syscat::name parent-type)
                                                             p1uid
                                                             (syscat::name pcrel)
                                                             (syscat::name child-type)
                                                             c1uid
                                                             (syscat::name cprel)))
                                               (format nil "~{/~A~}"
                                                       (list (syscat::name parent-type) p1uid))
                                               resourcetype-schema)
      ;; Verify that the child-to-parent back-link is present
      (syscat::log-message :debug "Verify that the child-to-parent back-link is present")
      (fiveam:is
        (integerp
          (syscat::resource-exists-p session (list (syscat::name parent-type)
                                                       p1uid
                                                       (syscat::name pcrel)
                                                       (syscat::name child-type)
                                                       c1uid
                                                       (syscat::name cprel)
                                                       (syscat::name parent-type)
                                                       p1uid))))
      (fiveam:is
        (= 1
           (length (syscat::get-resources session
                                              resourcetype-schema
                                              (list (syscat::name parent-type)
                                                    p1uid
                                                    (syscat::name pcrel)
                                                    (syscat::name child-type)
                                                    c1uid
                                                    (syscat::name cprel))))))
      ;; Create the grandchild resource, complete with back-link
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  p1uid
                                                  (syscat::name pcrel)
                                                  (syscat::name child-type)
                                                  c1uid
                                                  (syscat::name cgrel)
                                                  (syscat::name grandchild-type))
                                            `(("ScUID" . ,g1uid))
                                            *admin-user*)
      (syscat::create-relationship-by-path session
                                               (format nil "~{/~A~}"
                                                       (list (syscat::name parent-type)
                                                             p1uid
                                                             (syscat::name pcrel)
                                                             (syscat::name child-type)
                                                             c1uid
                                                             (syscat::name cgrel)
                                                             (syscat::name grandchild-type)
                                                             g1uid
                                                             (syscat::name gcrel)))
                                               (format nil "~{/~A~}"
                                                       (list (syscat::name parent-type)
                                                             p1uid
                                                             (syscat::name pcrel)
                                                             (syscat::name child-type)
                                                             c1uid))
                                               resourcetype-schema)
      (syscat::log-message :debug "Verify that the grandchild-to-child back-link is present")
      (fiveam:is
        (integerp
          (syscat::resource-exists-p session (list (syscat::name parent-type)
                                                       p1uid
                                                       (syscat::name pcrel)
                                                       (syscat::name child-type)
                                                       c1uid
                                                       (syscat::name cgrel)
                                                       (syscat::name grandchild-type)
                                                       g1uid
                                                       (syscat::name gcrel)
                                                       (syscat::name child-type)
                                                       c1uid))))
      ;; Clean-up: delete the resources
      (syscat::log-message :info ";TEST Cleaning up: removing the resources")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name parent-type) p1uid)
                                           resourcetype-schema
                                           :recursive t)
      ;; Delete the temporary schema-version
      (syscat::delete-schema-version session schema-version))))
