;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for sessions

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  sessions
  :description "Tests for session-management.")

(fiveam:in-suite sessions)


(fiveam:test
  authentication-and-sessions
  (let* ((username "Fishdude")
         (password1 "I like fish")
         (password2 "Luv 2 swim")
         (session-obj (make-instance 'syscat::session
                                     :username username
                                     :domain (syscat::domain *auth-server*)))
         (db-session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version db-session)))
    ;; Install the core schema
    (syscat::ensure-current-schema db-session syscat::*core-schema*)
    ;; Ensure the auth resources are present
    (syscat::install-auth-resources *auth-server*)
    ;; Confirm there's only one account on the auth server
    (let ((result (syscat::fetch-accounts *auth-server*)))
      (fiveam:is (= 1 (length result)))
      ;; ...and that it's the one we think it is
      (fiveam:is (equal "ScAdmin" (cdr (assoc "name" (car result) :test #'equal)))))
    ;; Fail to authenticate with that account
    (fiveam:is (not (syscat::check-user-creds *auth-server* username password1)))
    ;; Create the account
    (fiveam:is (syscat::add-account *auth-server* username password1))
    ;; Fail to create a duplicate account
    (fiveam:signals syscat::client-error
                    (syscat::add-account *auth-server* username password1))
    ;; Confirm there are now 2 accounts on the auth server
    (fiveam:is (= 2 (length (syscat::fetch-accounts *auth-server*))))
    ;; Validate the password
    (fiveam:is (syscat::check-user-creds *auth-server* username password1))
    ;; Fail to validate the wrong password
    (fiveam:is (not (syscat::check-user-creds *auth-server* username password2)))
    ;; Create a session for this account
    (fiveam:is (syscat::store-session *session-server* session-obj))
    ;; Confirm that the session is there and correct
    (let ((result (syscat::fetch-session *session-server*
                                             (syscat::identifier session-obj))))
      (fiveam:is (not (null result)))  ; We did *get* a session, yes?
      (fiveam:is (equal (syscat::identifier session-obj)
                        (syscat::identifier result)))
      (fiveam:is (equal (syscat::username session-obj)
                        (syscat::username result)))
      (fiveam:is (equal (syscat::domain session-obj)
                        (syscat::domain result))))
    ;; Delete the session
    (fiveam:is (syscat::delete-session *session-server*
                                           (syscat::identifier session-obj)))
    ;; Confirm the session is gone
    (fiveam:is (null (syscat::fetch-session *session-server* (syscat::identifier
                                                                   session-obj))))
    ;; Create a new session
    (fiveam:is (syscat::store-session *session-server* session-obj))
    ;; Change the password
    ;; Note that we only need to check this once here, not twice as in the client tests.
    ;; This is because the same internal function is called, regardless of whether
    ;; the user changes their own password, or the admin does it for them.
    (fiveam:is (syscat::change-password *auth-server* username password2))
    ;; Verify that the session was deleted when the password was changed
    (fiveam:is (null (syscat::fetch-session *session-server* (syscat::identifier
                                                                   session-obj))))
    ;; Validate the new password
    (fiveam:is (syscat::check-user-creds *auth-server* username password2))
    ;; Fail to validate the old password
    (fiveam:is (not (syscat::check-user-creds *auth-server* username password1)))
    ;; Create yet another session
    (fiveam:is (syscat::store-session *session-server* session-obj))
    ;; Delete the account
    (fiveam:is (syscat::delete-account *auth-server* username))
    ;; Fail to validate the new password
    (fiveam:is (not (syscat::check-user-creds *auth-server* username password2)))
    ;; Confirm the session is gone (again)
    (fiveam:is (null (syscat::fetch-session *session-server* (syscat::identifier
                                                                   session-obj))))
    ;; Confirm there's once again only one account on the auth server
    (let ((result (syscat::fetch-accounts *auth-server*)))
      (fiveam:is (= 1 (length result)))
      ;; ...and that it's the one we think it is
      (fiveam:is (equal "ScAdmin" (cdr (assoc "name" (car result) :test #'equal)))))
    ;; Delete the new schema-version
    (syscat::delete-schema-version db-session schema-version)))
