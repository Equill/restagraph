;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for side-effecting code pertaining mostly to resources.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  bolt-side-effecting-resources-canonical-paths
  :description "Tests for side-effecting code pertaining mostly to resources."
  :in main)

(fiveam:in-suite bolt-side-effecting-resources-canonical-paths)

(fiveam:test
  recanonicalise-one-resource
  "Test re-canonicalisation of a single resource."
  (let* ((uid "Clarion the Fluff")
         (sanitised-uid (syscat::sanitise-uid uid))
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (resourcetype-schema (car (syscat::fetch-current-schema session))))
    ;; Create the resource
    (fiveam:is (equal sanitised-uid
                      (syscat::store-resource session
                                                  resourcetype-schema
                                                  "People"
                                                  `(("ScUID" . ,uid))
                                                  *admin-user*)))
    ;; Delete its canonical path
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (p:People { ScUID: $ScUID }) REMOVE p.ScCanonicalpath"
      :parameters `(("ScUID" . ,sanitised-uid)))
    ;; Confirm it has no canonical-path attribute
    (fiveam:is (null
                 (cdr
                   (assoc "p.ScCanonicalpath"
                          (car
                            (neo4cl:bolt-transaction-autocommit
                              session
                              "MATCH (p:People { ScUID: $uid}) RETURN p.ScCanonicalpath"
                              :parameters `(("uid" . ,sanitised-uid))))))))
    ;; Canonicalise it
    (syscat::canonicalise-resource session
                                       (list "People" sanitised-uid)
                                       resourcetype-schema)
    ;; Confirm that it now *has* a canonical-path attribute
    (fiveam:is (string=
                 (format nil "/People/~A" sanitised-uid)
                 (cdr
                   (assoc "p.ScCanonicalpath"
                          (car
                            (neo4cl:bolt-transaction-autocommit
                              session
                              "MATCH (p:People { ScUID: $uid}) RETURN p.ScCanonicalpath"
                              :parameters `(("uid" . ,sanitised-uid))))
                          :test #'equal))))
    ;; Delete the resource
    (syscat::delete-resource-by-path session
                                         (list "People" sanitised-uid)
                                         resourcetype-schema)
    ;; Close the session
    (neo4cl:disconnect session)))

(fiveam:test
  recanonicalise-all-the-resources
  "Test re-canonicalisation of a single resource."
  (let* ((restype1 "People")
         (uid1 "Juan")
         (sanitised-uid1 (syscat::sanitise-uid uid1))
         (restype2 "Organisations")
         (uid2 "Juancorp")
         (sanitised-uid2 (syscat::sanitise-uid uid2))
         (restype3 "Buildings")
         (uid3 "Edificio de Juancorp")
         (sanitised-uid3 (syscat::sanitise-uid uid3))
         (deprel1 "CONTAINS_FLOORS")
         (deprestype1 "Floors")
         (depuid1 "Primera planta")
         (sanitised-depuid1 (syscat::sanitise-uid depuid1))
         (deprel2 "CONTAINS_ROOMS")
         (deprestype2 "Rooms")
         (depuid2 "Cocina")
         (deprel3 "HAS_COMMENT")
         (deprestype3 "Comments")
         (depresuid3 "This is a comment")
         (sanitised-depresuid3 (syscat::sanitise-uid depresuid3))
         (sanitised-depuid2 (syscat::sanitise-uid depuid2))
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (parsed-schema (syscat::parse-schema-from-alist
                          (json:decode-json-from-source #P"../test/internal_tests/test_schema.json")))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the test  schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (syscat::install-subschema session parsed-schema schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Create the resources
      (fiveam:is (equal sanitised-uid1
                        (syscat::store-resource session
                                                    resourcetype-schema
                                                    restype1
                                                    `(("ScUID" . ,uid1))
                                                    *admin-user*)))
      (fiveam:is (equal sanitised-uid2
                        (syscat::store-resource session
                                                    resourcetype-schema
                                                    restype2
                                                    `(("ScUID" . ,uid2))
                                                    *admin-user*)))
      (fiveam:is (equal sanitised-uid3
                        (syscat::store-resource session
                                                    resourcetype-schema
                                                    restype3
                                                    `(("ScUID" . ,uid3))
                                                    *admin-user*)))
      (fiveam:is (equal sanitised-depuid1
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list restype3 sanitised-uid3 deprel1 deprestype1)
                          `(("ScUID" . ,depuid1))
                          *admin-user*)))
      (fiveam:is (equal sanitised-depuid2
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list restype3
                                sanitised-uid3
                                deprel1
                                deprestype1
                                sanitised-depuid1
                                deprel2
                                deprestype2)
                          `(("ScUID" . ,depuid2))
                          *admin-user*)))
      (fiveam:is (equal sanitised-depresuid3
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list restype3
                                sanitised-uid3
                                deprel1
                                deprestype1
                                sanitised-depuid1
                                deprel2
                                deprestype2
                                sanitised-depuid2
                                deprel3
                                deprestype3)
                          `(("ScUID" . ,depresuid3))
                          *admin-user*)))
      ;; Delete their canonical paths
      (syscat::log-message :debug ";TEST Delete the canonical paths from the resources")
      (mapcar (lambda (path)
                (neo4cl:bolt-transaction-autocommit
                  session
                  (format nil "MATCH ~A REMOVE n.ScCanonicalpath"
                          (syscat::uri-node-helper path))))
              (list (list restype1 sanitised-uid1)
                    (list restype2 sanitised-uid2)
                    (list restype3 sanitised-uid3)
                    (list restype3 sanitised-uid3 deprel1 deprestype1 sanitised-depuid1)
                    (list restype3
                          sanitised-uid3
                          deprel1
                          deprestype1
                          sanitised-depuid1
                          deprel2
                          deprestype2
                          sanitised-depuid2)
                    (list restype3
                          sanitised-uid3
                          deprel1
                          deprestype1
                          sanitised-depuid1
                          deprel2
                          deprestype2
                          sanitised-depuid2
                          deprel3
                          deprestype3
                          sanitised-depresuid3)))
      ;; Confirm they have no canonical-path attribute
      (syscat::log-message :debug ";TEST Confirm the resources have no canonical-path attribute")
      (mapcar (lambda (path)
                (fiveam:is (null
                             (cdr
                               (assoc "ScCanonicalpath"
                                      (car
                                        (neo4cl:bolt-transaction-autocommit
                                          session
                                          (format nil "MATCH ~A RETURN n.ScCanonicalpath AS ScCanonicalpath"
                                                  (syscat::uri-node-helper path)))))))))
              (list (list restype1 sanitised-uid1)
                    (list restype2 sanitised-uid2)
                    (list restype3 sanitised-uid3)
                    (list restype3 sanitised-uid3 deprel1 deprestype1 sanitised-depuid1)
                    (list restype3
                          sanitised-uid3
                          deprel1
                          deprestype1
                          sanitised-depuid1
                          deprel2
                          deprestype2
                          sanitised-depuid2)
                    (list restype3
                          sanitised-uid3
                          deprel1
                          deprestype1
                          sanitised-depuid1
                          deprel2
                          deprestype2
                          sanitised-depuid2
                          deprel3
                          deprestype3
                          sanitised-depresuid3)))
      ;; Canonicalise all of them
      (syscat::log-message :debug ";TEST Recanonicalise all the resources")
      (syscat::recanonicalise-all-the-resources session resourcetype-schema)
      ;; Confirm that they now *have* a canonical-path attribute
      (syscat::log-message :debug ";TEST Confirming that the recanonicalised resources have a canonical-path attribute")
      (mapcar
        (lambda (path)
          (let ((result (neo4cl:bolt-transaction-autocommit
                          session
                          (format nil "MATCH ~A RETURN n.ScCanonicalpath AS ScCanonicalpath"
                                  (syscat::uri-node-helper path)))))
            (syscat::log-message :debug (format nil "Fetched details for ~A ~A: ~A"
                                                    restype1 sanitised-uid1 result))
            (fiveam:is (and
                         (stringp (cdr
                                    (assoc "ScCanonicalpath"
                                           (car result)
                                           :test #'equal)))
                         (string=
                           (format nil "~{/~A~}" path)
                           (cdr
                             (assoc "ScCanonicalpath"
                                    (car result)
                                    :test #'equal)))))))
        (list (list restype1 sanitised-uid1)
              (list restype2 sanitised-uid2)
              (list restype3 sanitised-uid3)
              (list restype3 sanitised-uid3 deprel1 deprestype1 sanitised-depuid1)
              (list restype3
                    sanitised-uid3
                    deprel1
                    deprestype1
                    sanitised-depuid1
                    deprel2
                    deprestype2
                    sanitised-depuid2)
              (list restype3
                          sanitised-uid3
                          deprel1
                          deprestype1
                          sanitised-depuid1
                          deprel2
                          deprestype2
                          sanitised-depuid2
                          deprel3
                          deprestype3
                          sanitised-depresuid3)))
      ;; Delete the resources
      (syscat::log-message :debug ";TEST Clean up the test resources and schema")
      (syscat::delete-resource-by-path session
                                           (list restype1 sanitised-uid1)
                                           resourcetype-schema)
      (syscat::delete-resource-by-path session
                                           (list restype2 sanitised-uid2)
                                           resourcetype-schema)
      (syscat::delete-resource-by-path session
                                           (list restype3 sanitised-uid3)
                                           resourcetype-schema
                                           :recursive t)
      ;; Clean up the test-schema we added
      (syscat::delete-schema-version session schema-version)
      ;; Close the session
      (neo4cl:disconnect session))))
