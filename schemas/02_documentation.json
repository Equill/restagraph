{
  "name": "documentation",
    "description": "Resourcetypes specifically for documenting things, plus their back-relationships.",
    "dependencies": [ "internet" ],
    "resourcetypes": [
      {
        "name": "Articles",
        "dependent": false,
        "description": "May be published on paper, or as a web-page or blogpost.",
        "attributes": [
          {
            "name": "Summary",
            "description": "The gist of the article, or an abstract of it. If you're just copying the entire thing, put it in the 'Text' attribute.",
            "type": "text"
          },
          {
            "name": "Text",
            "description": "The text of the article, in the event that you transcribe the entire thing.",
            "type": "text"
          },
          {
            "name": "Url",
            "description": "The URL from which this specific article can be (or was) fetched. If it's a sub-page of a website, don't put it here; instead, create a Websites resource for it and link the two together."
          }
        ]
      },
      {
        "name": "Ways",
        "dependent": false,
        "description": "Description of a way to do something. Formerly known as Howtos or HOWTOs, a term which implies a single, definitive approach. This name is less prescriptive and encourages exploration and inclusion of other approaches.",
        "attributes": [
          {
            "name": "Objective",
            "description": "The intended outcome.",
            "type": "text"
          },
          {
            "name": "AssumedKnowledge",
            "description": "What you're assumed to already know before attempting this task.",
            "type": "text"
          },
          {
            "name": "Prerequisites",
            "description": "Things that already need to be true before you start.",
            "type": "text"
          },
          {
            "name": "DecisionsToMake",
            "description": "Things you need to decide. It's better if you make these decisions ahead of time.",
            "type": "text"
          },
          {
            "name": "Instructions",
            "description": "How to do it.",
            "type": "text"
          }
        ]
      },
      {
        "name": "Topics",
        "dependent": false,
        "description": "A subject area, that things can be said to relate to.",
        "attributes": [
          {
            "name": "Notes",
            "description": "Elaborating notes about what this topic is and is not about.",
            "type": "text"
          }
        ]
      },
      {
        "name": "Wikipages",
        "dependent": "false",
        "description": "generic wikipage",
        "attributes": [
          {
            "name": "title",
            "description": "In case you want to use something other than an escaped version of the UID",
            "type": "varchar"
          },
          {
            "name": "text",
            "description": "The actual text of the page.",
            "type": "text"
          }
        ]
      }
    ],
    "relationships": [
      {
        "name": "AUTHOR_OF",
        "source-types": [ "People" ],
        "target-types": [ 
          "Articles",
          "Files"
        ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "This person is the/an author of that resource.",
        "complemented_by": "HAS_AUTHOR"
      },
      {
        "name": "HAS_AUTHOR",
        "source-types": [
          "Articles",
          "Files"
        ],
        "target-types": [ "People" ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "That person is the/an author of this resource.",
        "complemented_by": "AUTHOR_OF"
      },
      {
        "name": "HAS_TOPIC",
        "source-types": [
          "Articles",
          "Commentary",
          "Files",
          "Ideas",
          "Websites",
          "Wikipages"
        ],
        "target-types": [
          "Topics"
        ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "This resource addresses that topic.",
        "complemented_by": "TOPIC_OF"
      },
      {
        "name": "HOSTED_ON",
        "source-types": [ "Articles" ],
        "target-types": [ "Websites" ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "This article is found on that site.",
        "complemented_by": "HOST_OF"
      },
      {
        "name": "HOST_OF",
        "source-types": [ "Websites" ],
        "target-types": [ "Articles" ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "That article is found on this site.",
        "complemented_by": "HOSTED_ON"
      },
      {
        "name": "TOPIC_OF",
        "source-types": [ "Topics" ],
        "target-types": [
          "Articles",
          "Commentary",
          "Files",
          "Ideas",
          "Websites",
          "Wikipages"
        ],
        "cardinality": "many:many",
        "reltype": "any",
        "description": "That resource is about this general subject area.",
        "complemented_by": "HAS_TOPIC"
      }
    ]
}
