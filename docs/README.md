# Syscat documentation

This is a quick overview, to help you pick which document to read if you're not already familiar with it all.


## Conceptual overview

If you're getting to know Syscat for the first time, `Conceptual_overview.md` is the best place to start.

It describes the thinking that the APIs express, making it a heck of a lot easier to understand them.


## API references

These cover the HTTP APIs themselves, in terms of both how they work, and of the thinking that underlies it.

These *don't* cover specific use-cases.
