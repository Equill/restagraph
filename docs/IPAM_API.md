# IPAM API

IP Address Management is a complex thing. Although *technically* you could do this stuff via the Raw API, it's difficult to co-ordinate the necessary moving parts, and easy to get it wrong. Hence the specialised API that takes care of the fiddly details for you.

This was designed by a network engineer, for other network engineers, so this page assumes you have at least a general understanding of managing TCP/IP networks in a commercial (or commercial-like) environment. In keeping with section 2(4) of [RFC 1925](https://datatracker.ietf.org/doc/html/rfc1925), much of what follows will only make sense if you've managed one.

If this document makes no sense to you, either you don't need this functionality and can ignore these APIs, or you're in for quite a learning experience.

This was actually built for Syscat, but it eventually made most sense to just fold the functionality into the core product and make it available to everybody who uses Syscat. For the full context of how this was intended to be used (assuming you didn't come here from there in the first place) check out [Syscat](https://codeberg.org/Equill/syscat/) - you might be glad you did :)


## Conceptual overview

IP network and address allocation is surprisingly difficult to get right. It gets even more complicated when you try to keep track of private address space across multiple organisations.

The subnet API automatically takes care of subnetting, and of merging/rebalancing subnets and address allocations when you delete a subnet that has subnets of its own.

The addresses API takes care of figuring out where to attach addresses as you allocate them. It's not separate for any ideological reason, but for the purely practical one that I haven't figured out how to manage them under the same endpoint.


### Allocated vs configured

It's important to bear in mind the distinction between _allocated_ subnets and addresses, and _configured_ addresses.

Allocated means "I _intend_ for this subnet to be used over here, and for this address within that subnet to be used on this interface of that device."

Configured means "this interface answers to traffic on that address, and possibly also sends traffic from it." Although a netmask is configured along with the address, the TCP/IP stack only uses it for filtering and routing - it carries no meaning, as such.

Despite the fervent insistence by many people, these are not the same thing - and there's absolutely no guarantee that what's configured is what was allocated. The distinction is equally important with VRFs and VLANs - the actually configured values can be wildly different to those on the design documents, yet act in a *functionally* equivalent manner.

This functionality is about _allocating_ networks and addresses for use - it's about what's _intended_. If you want to record what address is _configured_ on an interface, the Raw API serves just fine.


# Subnets API

All operations apply the same to both IPv4 and IPv6 subnets.

Where this document refers to CIDR format, it means the following style:

- IPv4: `192.0.2.0/24`
- IPv6: `2001:db8::/32`

IPv6 addresses can be supplied in this shortened form, but Syscat always stores and returns them full-length.


## POST

Create a subnet.

If there is an existing subnet that will serve as a supernet of the one you're creating, the server will automatically re-home subnets and IP addresses that belong underneath this one.


```
POST /ipam/v1/subnets
```

Required parameters:

- `subnet`
    - Either IPv4 or IPv6, in CIDR format.
- `org`
    - Must be the UID for an organisation that already exists in the database.


Optional parameters:

- `vrf`
    - Relevant if you've defined one or more VRF-groups under the organisation, and want to allocate this subnet within one of them.


Return value:

- Status code: 201
- Body: The canonical URI for the newly-created subnet


Example:

Create a new /24 subnet, having already created a /8 that fits as a supernet of the new one.

```
$ curl -X POST -d 'org=MyCompany' -d 'subnet=10.255.0.0/24' http://192.0.2.1:4950/ipam/v1/subnets

/Organisations/MyCompany/SUBNETS/Ipv4Subnets/10.255.0.0_8/SUBNETS/Ipv4Subnets/10.255.0.0_24
```


## GET

Search for a subnet, returning its canonical path.

```
GET /ipam/v1/subnets
```

Required parameters:

- `subnet`
    - Either IPv4 or IPv6, in CIDR format.
- `org`


Optional parameters

- `vrf`


Return value: The canonical path for the subnet, e.g. `/Organisations/MyCompany/SUBNETS/Ipv4Subnets/10.255.0.0_8/SUBNETS/Ipv4Subnets/10.255.0.1_24`



## DELETE

Delete a subnet.

As with creating subnets, if you're removing a subnet that has a parent supernet, the server will re-home the subnets and addresses underneath it.

Caveat: If the requested subnet directly contains IP addresses and is at the top level, i.e. directly below the organisation or VRF-group, the server will refuse to delete it. You have to delete the addresses first, effectively de-allocating them, because it doesn't make sense to allocate bare IP addresses without a subnet to contain them.


```
GET /ipam/v1/subnets
```

Required parameters:

- `subnet`
    - Either IPv4 or IPv6, in CIDR format.
- `org`


Optional parameters:

- `vrf`


Return values:

- Status code: 204
- Body: ""



# Addresses API

This works in a very similar way to the Subnets API, except you don't specify the prefix-length.


## POST

Create an address.

```
POST /ipam/v1/addresses
```


Required parameters:

- `address`
    - A bare IP address, i.e. no prefix-length or other indicator of its subnet. Syscat will figure that out.
- `org`


Optional parameters:

- `vrf`



Return values:

- Status code: 201
- Body: The canonical URI for the newly-created address, e.g. `/Organisations/MyCompany/SUBNETS/Ipv4Subnets/10.255.0.0_8/SUBNETS/Ipv4Subnets/10.255.0.1_24/ADDRESSES/Ipv4Addresses/10.255.0.9`


## GET

Search for an address

```
GET /ipam/v1/addresses
```


Required parameters:

- `address`
    - A bare IP address, i.e. no prefix-length or other indicator of its subnet. Syscat will figure that out.
- `org`


Optional parameters

- `vrf`


Return values:

- Status code: 200
- Body: The canonical URI for the address, e.g. `/Organisations/MyCompany/SUBNETS/Ipv4Subnets/10.255.0.0_8/SUBNETS/Ipv4Subnets/10.255.0.1_24/ADDRESSES/Ipv4Addresses/10.255.0.9`


## DELETE

Delete an address entry

```
DELETE /ipam/v1/addresses
```

Required parameters:

- `address`
    - A bare IP address, i.e. no prefix-length or other indicator of its subnet. Syscat will figure that out.
- `org`


Optional parameters:

- `vrf`


Return values:

- Status code: 204
- Body: ""
