# Files API

This is the API for uploading, downloading and deleting files; these operations cannot be done via the Raw API, though you *can* use it to query and connect to their metadata.

Three methods are supported:
- POST
- GET
- DELETE


# POST: Upload a file

This requires the use of a form-style multipart request, to upload binary data.

In Python's `request` module, for example, you'd use the `files` parameter with the `post` method. In `curl`, we use the `-F` flag.

```
POST /files/v1
```

Mandatory parameters:

- `file`
    - The file itself.
- `name`
    - The name of the file, i.e. the UID that should be used for its metadata.


Return values:

- Status code: 201
- Body: the URI for the file's metadata, in the form `/Files/<sanitised name>`.
    - This assumes that you'll have more need to interact with the file's metadata than with the file itself.


Return header:

- `Location` = the URI of the new file.
    - This is exactly the same as the URI returned in the body.
    - It's included to make life easier for automation tooling.


Curl example:

```
curl -F "file=@/path/to/file.jpg" -F "name=NameOfMyFile" http://sctest.onfire.onice/files/v1/
```


Python example:

```
import requests

fhandle = open("/path/to/file.jpg", 'rb')
requests.post('http://sctest.onfire.onice/files/v1/', data={'name': "Name of the file"}, files={'file': fhandle})
fhandle.close()
```


# GET: Download a file

```
GET /files/v1/<UID>
```


Return values:

- Status code: 200
- Body: the file itself


Headers:

- `Content-Type`: the mediatype/MIME-type as detected by the server when the file was uploaded 


Note that the file's metadata can be retrieved via the Raw API, using the path `/raw/v1/Files/<uid of the file>`.


# DELETE: Remove a file

```
DELETE /files/v1/<UID>
```

Return values:

- Status code: 204
- Body: ""


This will also automatically delete the resource containing the file's metadata, along with any relationships that had with other resources.

Note that the Raw API will reject any attempt to delete a `Files` resource. It can only be deleted from here.
