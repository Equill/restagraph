Installation instructions for Syscat

# Tarball release with systemd

The following instructions assume a default installation, and are performed from the command-line.

## Prerequisites

- Neo4j must already be installed and running.


## Installation procedure

Note that the server doesn't care what you call it or what user, group or directories are involved, or what port it listens on, as long as they match the configuration. Change these values to suit your needs.

- Download the tarball from [Codeberg](https://codeberg.org/Equill/syscat/releases).
    - For the purpose of this document, I'll assume you downloaded it to `/tmp/syscat_0.15.4.tar.gz`
- Create the user and group under which you intend to run the app.
    - `sudo groupadd syscat`
    - `sudo useradd -g syscat -s /bin/bash syscat`
- Create the parent directories for the app and its data, and set their ownership accordingly:
    - `sudo mkdir /opt/syscat && sudo chown syscat:syscat /opt/syscat`
    - `sudo mkdir /var/opt/syscat && sudo chown syscat:syscat /var/opt/syscat`
- Uncompress the tarball into the install directory as the runtime user
    - `cd /opt/syscat && sudo -u syscat tar zxf /tmp/syscat_0.15.4.tar.gz`
- While still in that directory, set the `current` symbolic link, to make this the "current" version:
    - `sudo -u syscat ln -s syscat_0.15.4.tar.gz current`
    - You don't *have* to use this approach, but it's a much easier way of upgrading, rolling back to a previous version, and verifying which version you're actually running.
- Copy the `systemd` service definition into place, and edit it accordingly.
    - `sudo cp /opt/syscat/current/syscat.service /etc/systemd/system/`
    - At the least, you'll need to set the Neo4j password to match the one you set on the server.


## Upgradee a tarball release

This is basically a subset of the install instructions.

- Download the tarball for the new release.
- Move into the install director.
    - `cd /opt/syscat`
- Uncompress the new tarball, as the target user.
    - `tar zxf /tmp/syscat_0.15.5.tar.gz`
- Remove and replace the `current` symlink.
    - `sudo rm current && sudo -u syscat ln -s syscat_0.15.5 current`
- Restart the service.
    - `sudo systemd restart syscat`
