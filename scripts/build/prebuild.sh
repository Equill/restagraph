# Establish the version suffix
if [[ -z $1 ]]
then
  SUFFIX="snapshot"
else
  SUFFIX=${1}
fi

NOCACHE=true
docker build -f Dockerfile-prebuild --no-cache=$NOCACHE --progress=plain -t equill/syscat-prebuild:$SUFFIX ./
