sbcl --core /home/builder/prebuild \
    --userinit /home/builder/sbclrc \
    --eval "(asdf:clear-source-registry)" \
    --eval "(asdf:load-system :syscat)" \
    --eval "(sb-ext:save-lisp-and-die \"syscat\" :executable t :toplevel #'syscat:dockerstart)"
