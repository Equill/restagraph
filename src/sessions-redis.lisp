;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Classes and methods for managing sessions via Redis

(in-package :syscat)


;;; Redis-specific methods

(defclass redis-server ()
  ((host :initarg :host
         :accessor host
         :type string
         :initform "192.0.2.1")
   (port :initarg :port
         :accessor port
         :type integer
         :initform 6379))
  (:documentation "Details necessary for connecting to a Redis server."))


(defmethod check-session-server ((server redis-server))
  "Health-check of the Redis server. Returns a boolean indicating success/failure."
  (redis:connect :host (host server) :port (port server))
  (if
    (redis:red-ping)
    (progn (redis:disconnect) t)
    (redis:disconnect)))

(defmethod store-session ((server redis-server) (session session))
  (when (equal "ScSystem" (username session))
    (error "Refusing to store a session for ScSystem"))
  (redis:with-connection (:host (host server) :port (port server))
                         (redis:red-save)
                         (redis:red-hmset (identifier session)
                                          "username" (username session)
                                          "domain" (domain session))))

(defun list-to-plist (lst &optional acc)
  "Helper function, to make the return value of redis:red-hgetall useful.
  Takes a list of even-numbered length and returns a plist,
  assuming the odd-numbered elements are strings."
  (unless (zerop (mod (length lst) 2))
    (error "Odd number of elements in the list."))
  ;; Have we reached the end of the list?
  (if (null lst)
    ;; End of list; return the hash
    acc
    ;; Not end of list. Append the next pair to the accumulator and go back around
    (list-to-plist
      (cddr lst)
      (append acc (list (intern (string-upcase (first lst)) :keyword)
                        (second lst))))))

(defmethod fetch-session ((server redis-server) (session-id string))
  (log-message :debug (format nil "Attempting to fetch details of session '~A'" session-id))
  (let ((returnval nil))
    (redis:with-connection
      (:host (host server) :port (port server))
      (redis:red-save)
      ;; FIXME: we're making some serious assumptions here
      (let ((result (list-to-plist (redis:red-hgetall session-id))))
        (if result
          (progn
            (log-message :debug (format nil "Session '~A' found." session-id))
            (setf returnval (make-instance 'session
                                           :identifier session-id
                                           :username (getf result :username)
                                           :domain (getf result :domain))))
          (log-message :debug (format nil "Session '~A' not found" session-id)))))
    returnval))

(defmethod delete-session ((server redis-server) (session-id string))
  (redis:with-connection (:host (host server) :port (port server))
    (redis:red-del session-id)))
