;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; The HTTP API server application

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


;;; Define a logging method
(defmethod tbnl:acceptor-log-message ((acceptor syscat-acceptor)
                                      log-level
                                      format-string
                                      &rest format-arguments)
  (log-message log-level (append (list format-string) format-arguments)))

;;; Configure Hunchentoot to extract POST-style parameters
;;; when processing PUT and DELETE requests
(pushnew :PUT tbnl:*methods-for-post-parameters*)
(pushnew :DELETE tbnl:*methods-for-post-parameters*)


;;; Appserver startup/shutdown

(defun startup (&key acceptor dispatchers docker)
  "Start up the appserver.
  Ensures the uniqueness constraint on resource-types is present in Neo4j.
  Keyword arguments:
  - acceptor = prebuilt acceptor, to use instead of the default.
  - dispatchers = extra dispatchers to add to tbnl:*dispatch-table* in addition to the defaults.
  - docker = whether to start up in a manner suitable to running under docker,
  i.e. return only after Hunchentoot shuts down, instead of immediately after it starts up."
  (declare (type (boolean) docker))
  ;; Set debug logging, if requested
  (when (sb-ext:posix-getenv "DEBUG")
    (setf *loglevel* :debug))
  ;; Diagnostics
  (log-message :debug (format nil "Shell search path: ~A" (sb-ext:posix-getenv "PATH")))
  (log-message :info "Attempting to start up the syscat application server")
  ;; Control the decoding of JSON identifiers
  (setf JSON:*JSON-IDENTIFIER-NAME-TO-LISP* 'common-lisp:string-upcase)
  ;; Sanity-check: is an acceptor already running?
  ;;; We can't directly check whether this acceptor is running,
  ;;; so we're using the existence of its special variable as a proxy.
  (handler-case
    (if *syscat-acceptor*
      ;; There's an acceptor already in play; bail out.
      (log-message :warn "Acceptor already exists; refusing to create a new one.")
      ;; No existing acceptor; we're good to go.
      (progn
        ;; Ensure we have an acceptor to work with
        (when (null acceptor) (setf acceptor (make-default-acceptor)))
        ;; Make it available as a dynamic variable, for shutdown to work on
        (defparameter *syscat-acceptor* acceptor)
        ;; Sanity-check: do we have a storage directory?
        (log-message :info (format nil "Ensuring file-storage location '~A' is present"
                                   (files-location *syscat-acceptor*)))
        (ensure-directories-exist (files-location *syscat-acceptor*))
        (unless (probe-file (files-location *syscat-acceptor*))
          (error (format nil "File storage location ~A does not exist!"
                         (files-location *syscat-acceptor*))))
        ;; Sanity-check whether the database is available
        (unless (confirm-db-is-running (datastore acceptor) :max-count 25)
          (error "Database is not available"))
        ;; Ensure there's a schema and the default set of resources
        (let ((session (neo4cl:establish-bolt-session (datastore acceptor))))
          (ensure-current-schema session *core-schema*)
          (let ((schemas (fetch-current-schema session)))
            (setf (resourcetype-schema acceptor) (car schemas))
            (setf (relationship-schema acceptor) (cdr schemas)))
          ;; Install the default resources
          (install-default-resources session (resourcetype-schema acceptor))
          (neo4cl:disconnect session))
        ;; Ensure the necessary authentication resources are present
        (install-auth-resources (auth-server *syscat-acceptor*))
        ;; Set the dispatch table
        (log-message :info "Configuring the dispatch table")
        (setf tbnl:*dispatch-table*
              (append
                ;; Syscat defaults
                (list (tbnl:create-prefix-dispatcher "/healthcheck" 'healthcheck-dispatcher)
                      (tbnl:create-prefix-dispatcher (uri-base-api acceptor) 'api-dispatcher-v1)
                      (tbnl:create-prefix-dispatcher (uri-base-schema acceptor) 'schema-dispatcher-v1)
                      (tbnl:create-prefix-dispatcher (uri-base-files acceptor) 'files-dispatcher-v1)
                      (tbnl:create-prefix-dispatcher (uri-base-subnets acceptor) 'subnet-dispatcher-v1)
                      (tbnl:create-prefix-dispatcher (uri-base-addresses acceptor) 'address-dispatcher-v1)
                      (tbnl:create-prefix-dispatcher "/auth/v1" 'auth-dispatcher-v1))
                ;; Include the additional dispatchers here
                dispatchers
                ;; Default fallback
                (list (tbnl:create-prefix-dispatcher "/" 'four-oh-four))))
        ;; Prepare for file upload
        (log-message :info "Ensuring the file-upload temp directory is present")
        (let ((files-temp-location (if (sb-ext:posix-getenv "FILES_TEMP_LOCATION")
                                     (sb-ext:posix-getenv "FILES_TEMP_LOCATION")
                                     "/tmp/syscat-files-tmp/")))
          (ensure-directories-exist files-temp-location)
          (setf tbnl:*tmp-directory* files-temp-location))
        ;; Recanonicalise all the resources, if the appropriate environment variable is set
        (if (uiop:getenvp "SC_RECANONICALISE")
          (let ((db-session (neo4cl:establish-bolt-session (datastore *syscat-acceptor*))))
            (log-message :info "Re-canonicalising all resources, per request of the admin.")
            (recanonicalise-all-the-resources db-session (resourcetype-schema *syscat-acceptor*))
            (neo4cl:disconnect db-session))
          ;; For debugging, positively confirm that no recanonicalisation request was detected
          (log-message :debug "Re-canonicalisation was not requested. Skipping this step."))
        ;; Start up the server
        (log-message :info "Starting up Hunchentoot to serve HTTP requests")
        (handler-case
          (tbnl:start acceptor)
          (usocket:address-in-use-error
            () (log-message :error
                            (format nil "Attempted to start an already-running instance!"))))
        (when docker
          (sb-thread:join-thread
            (find-if
              (lambda (th)
                (string= (sb-thread:thread-name th)
                         (format nil "hunchentoot-listener-~A:~A"
                                 (tbnl:acceptor-address acceptor)
                                 (tbnl:acceptor-port acceptor))))
              (sb-thread:list-all-threads))))))
    (misconfiguration
      (e)
      (if (equal (message e) "No such policy")
        (progn
          (log-message :fatal (format nil "Unrecognised policy-set ~A"
                                      (sb-ext:posix-getenv "ACCESS_POLICY")))
          (sb-ext:exit))
        (progn
          (log-message :fatal (format nil "Unhandled error in startup: ~A" (message e)))
          (sb-ext:exit))))))

(defun dockerstart ()
  (startup :docker t))

(defun shutdown ()
  ;; Check whether there's something to shut down
  (if *syscat-acceptor*
      ;; There is; go ahead
      (progn
      ;; Check whether it's still present but shutdown
      (if (tbnl::acceptor-shutdown-p *syscat-acceptor*)
          (log-message :info "Acceptor was present but already shut down.")
          (progn
            (log-message
              :info
              (format nil "Shutting down the syscat application server"))
            (handler-case
              ;; Perform a soft shutdown: finish serving any requests in flight
              (tbnl:stop *syscat-acceptor* :soft t)
              ;; Catch the case where it's already shut down
              (tbnl::unbound-slot
                ()
                (log-message
                  :info
                  "Attempting to shut down Hunchentoot, but it's not running."))
              (tbnl:hunchentoot-error
                ()
                (log-message :warn "Error from Hunchentoot server."))
              (sb-pcl::no-applicable-method-error
                ()
                (log-message
                  :info
                  "Attempted to shut down Hunchentoot, but received an error. Assuming it wasn't running.")))))
        ;; Nuke the acceptor
        (setf *syscat-acceptor* nil))
      ;; No acceptor. Note the fact and do nothing.
      (log-message :warn "No acceptor present; nothing to shut down.")))
