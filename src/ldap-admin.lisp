;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; LDAP subdomain management
;;; Depends on auth-ldap.lisp

(in-package :syscat)

(defgeneric list-authserv-contents (config &key username password)
  (:documentation "Dump the contents of the authentication server.
                   Useful for troubleshooting during development,
                   but under no circumstances should this be connected to an API."))

(defmethod list-authserv-contents ((config ldap-server) &key username password)
  ;; The ldap:ldap object must be created _before_ it's used.
  (let ((server (make-ldap-server config username password)))
    (ldap:ldif-search server "(objectclass=*)" :base (base-dn config))))

(defgeneric add-root-domain (config &key username password)
  (:documentation "Speculative function to add the initial root domain"))

(defmethod add-root-domain ((config ldap-server) &key username password)
  (let ((server (make-ldap-server config username password)))
    (if (ldap:search server
                     (format nil "(~A)" (base-dn config))
                     :base (base-dn config))
        (log-message :warn (format nil "Base DN ~A already exists." (base-dn config)))
        (ldap:add (ldap:new-entry (base-dn config)
                                  :attrs '((:OBJECTCLASS . ("dcObject" "organization"))
                                           (:O . "Root domain")))
                  server))))

(defgeneric add-subdomain (config subdomain username password)
  (:documentation "Add a subdomain, e.g. add dc=webcat to dc=onfire,dc=onice, and auto-add ou=users under it.
                   The result is that we now have ou=users,dc=webcat,dc=onfire,dc=onice and can start adding users to it."))

(defmethod add-subdomain ((config ldap-server) (subdomain string) (username string) (password string))
  (let ((server (make-ldap-server config username password)))
    (ldap:bind server)
    ;; Add the subdomain
    (ldap:add
      (ldap:new-entry (format nil "dc=~A,~A" subdomain (base-dn config))
                      :attrs '((:objectclass . ("dcObject" "organization"))
                               ;; o is a required attribute
                               (:o . "New subdomain")))
      server)
    ;; Add ou=users underneath it
    (ldap:add
      (ldap:new-entry (format nil "ou=users,dc=~A,~A" subdomain (base-dn config))
                      :attrs '((:objectclass . ("organizationalUnit"))))
      server)
    ;; Clean up the connection
    (ldap:unbind server)))


(defgeneric add-subdomain-user (config subdomain new-user username password)
  (:documentation "Add a user to the newly-created subdomain"))

(defmethod add-subdomain-user ((config ldap-server)
                               (subdomain string)
                               (new-user string)
                               (username string)
                               (password string))
  (let ((server (make-ldap-server config username password)))
    (ldap:bind server)
    ;; Add the subdomain
    (ldap:add
      (ldap:new-entry (format nil "cn=~A,ou=usersdc=~A,~A" new-user subdomain (base-dn config))
                      :attrs '((:objectclass . ("inetorgperson"))
                               ))
      server)
    ;; Clean up the connection
    (ldap:unbind server)))


(defgeneric set-subdomain-user-password (config subdomain new-user new-password username password)
  (:documentation "Set the password for a subdomain user."))

(defmethod set-subdomain-user-password ((config ldap-server)
                                        (subdomain string)
                                        (new-user string)
                                        (new-password string)
                                        (username string)
                                        (password string))
  (let ((server (make-ldap-server config username password)))
    (ldap:bind server)
    ;; Set the user's password
    (ldap:modify
      server
      (format nil "cn=~A,ou=usersdc=~A,~A" new-user subdomain (base-dn config))
      (list "userPassword" new-password))
    ;; Clean up the connection
    (ldap:unbind server)))
