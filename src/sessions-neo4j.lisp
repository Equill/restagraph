;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Classes and methods for managing sessions via Neo4j
;;; Depends on the auth-bolt-server class defined in auth-neo4j.lisp

(in-package :syscat)


(defmethod check-session-server ((server auth-bolt-server))
  (log-message :debug "Checking the health of the session-management server.")
  (let ((session (neo4cl:establish-bolt-session server)))
    (neo4cl:bolt-transaction-autocommit
      session
      "RETURN true")
    (neo4cl:disconnect session))
  ;; Explicitly return something positive
  t)

(defmethod store-session ((server auth-bolt-server)
                          (session session))
  (log-message :debug (format nil "Storing session '~A' for user '~A' in domain '~A'"
                              (identifier session)
                              (username session)
                              (domain server)))
  (when (equal "ScSystem" (username session))
    (error "Refusing to store a session for ScSystem"))
  (let ((db-session (neo4cl:establish-bolt-session server)))
    (neo4cl:bolt-transaction-autocommit
      db-session
      "MATCH (d:Domain {name: $domain}), (a:Account {name: $username}) CREATE (d)-[:HAS]->(:Session {sessionid: $sessionid})<-[:HAS]-(a)"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,(username session))
                    ("sessionid" . ,(identifier session))))
    (neo4cl:disconnect db-session))
  ;; Explicitly return something positive
  t)

(defmethod fetch-session ((server auth-bolt-server)
                          (session-id string))
  (let* ((db-session (neo4cl:establish-bolt-session server))
         (result (neo4cl:bolt-transaction-autocommit
                   db-session
                   "MATCH (:Domain {name: $domain})-[:HAS]->(:Session {sessionid: $sessionid})<-[:HAS]-(a:Account) RETURN a.name AS name"
                   :parameters `(("domain" . ,(domain server))
                                 ("sessionid" . ,session-id)))))
    ;; Clean up the connection as soon as it's no longer needed
    (neo4cl:disconnect db-session)
    ;; Now extract the result
    (log-message :debug (format nil "Fetched results ~A" result))
    (when result
      (make-instance 'session
                     :identifier session-id
                     :domain (domain server)
                     :username (cdr (assoc "name" (car result) :test #'equal))))))

(defmethod delete-session ((server auth-bolt-server)
                           (session-id string))
  (log-message :debug (format nil "Attempting to delete session '~A' for domain '~A'"
                              session-id (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (d:Domain {name: $domain})-[:HAS]->(s:Session {sessionid: $sessionid}) DETACH DELETE s"
      :parameters `(("domain" . ,(domain server))
                    ("sessionid" . ,session-id)))
    (neo4cl:disconnect session))
  ;; Explicitly return something positive
  t)
