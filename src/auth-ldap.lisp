;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;;;; Authentication via LDAP

(in-package :syscat)


;;; Classes

(defclass ldap-server ()
  ((host :initarg :host
         :accessor host
         :type string
         :initform "192.0.2.1"
         :documentation "Address on which the LDAP server is listening.")
   (port :initarg :port
         :reader port
         :type integer
         :initform 389
         :documentation "Port on which the LDAP server is listening.")
   (base-dn :initarg :base-dn
            :reader base-dn
            :type string
            :initform "dc=subdomain,dc=example,dc=com"
            :documentation "The base or root of the server, e.g. dc=example,dc=com")
   (sslflag :initarg :sslflag
            :reader sslflag
            :type boolean
            :initform nil
            :documentation "Whether to use TLS when connecting to the server."))
  (:documentation "Yer basic LDAP server, for authenticating against."))


;;; Helper functions

(defun make-ldap-server (config username password &key debug)
  "Convenience function to simplify initialisation of an ldap:ldap object."
  (declare (type ldap-server config))
  (ldap:new-ldap :host (host config)
                 :port (port config)
                 :sslflag (sslflag config)
                 :base (base-dn config)
                 :user (format nil "cn=~A,ou=users,~A"
                               username (base-dn config))
                 :pass password
                 :debug (when debug t)))

(defparameter *ldap* (make-instance 'ldap-server
                                    :port 1389
                                    :base-dn "dc=example,dc=onfire,dc=onice"))


;;; Methods

(defmethod check-user-creds ((config ldap-server)
                             (username string)
                             (password string))
  (log-message :debug (format nil "Checking credentials for user '~A'" username))
  (cond
    ((equal "ScSystem" username)
     (error "Not permitted to log in as ScSystem"))
    (config
      (let ((server (make-ldap-server
                      (make-instance 'ldap-server
                                     :host (host config)
                                     :port (port config)
                                     :base-dn (base-dn config))
                      username
                      password)))
        (log-message :debug (format nil "Attempting to bind to server ldap://~A:~D~%"
                                    (host config)
                                    (port config)))
        ;; Attempt to bind with those credentials
        (if (ldap:bind server)
          ;; If it worked, close the binding and indicate success
          (progn
            (ldap:unbind server)
            (log-message :debug (format nil "Bind succeeded"))
            t)
          ;; Didn't work.
          (log-message :debug (format nil "Bind failed")))))
    (t
      (error "No LDAP server defined!"))))


(defgeneric delete-entry (config dn &key username password)
  (:documentation "Delete the entry corresponding to the given DN.
                   Is possibly a little LDAP-specific."))

(defmethod delete-entry ((config ldap-server) (dn string) &key username password)
  (let ((server (make-ldap-server config username password)))
    (ldap:bind server)
    (ldap:delete server dn)
    (ldap:unbind server)))


(defmethod add-account ((server ldap-server)
                        (username string)
                        (password string))
  (let ((server (make-ldap-server server username password))
        (subdomain (second (cl-ppcre:split "="
                                           (first (cl-ppcre:split "," (base-dn server)))))))
    ;; First, check whether this account already exists
    (if (ldap:search
          server
          (format nil "cn=~A,ou=users,dc=~A,~A"
                  username subdomain (base-dn server)))
      ;; It exists; let me stop you right there.
      (error 'client-error :message
             (format nil "Domain '~A' already contains an account with name '~A'"
                     (domain server) username))
      ;; Doesn't already exist; knock yerself out.
      (progn
        (ldap:bind server)
        (ldap:add
          (ldap:new-entry (format nil "cn=~A,ou=users,dc=~A,~A"
                                  username subdomain (base-dn server))
                          ;; inetOrgPerson: https://docs.ldap.com/specs/rfc2798.txt
                          :attrs `((:objectclass . ("inetOrgPerson"))
                                   ;; Password must be pre-hashed
                                   (:userpassword . ,password)
                                   ;; sn is a required attribute
                                   (:sn "Surname field is unused")))
          server)
        ;; Clean up the connection
        (ldap:unbind server))))
  ;; If we got this far, return something explicitly positive
  t)

(defmethod change-password ((config ldap-server)
                            (username string)
                            (password string))
  (let ((server (make-ldap-server config username password)))
    (ldap:bind server)
    (ldap:modify server
                 (format nil "cn=~A,ou=users,~A" username (base-dn config))
                 ;; Remember the password must be pre-hashed
                 `((ldap:replace "userpassword" ,password)))
    ;; Clean up the connection
    (ldap:unbind server)))
