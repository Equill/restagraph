;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Authentication and user-management via Neo4j

(in-package :syscat)


;;; Classes

(defclass auth-bolt-server (neo4cl:bolt-server)
  ((domain :initarg :domain
           :reader domain
           :type string
           :initform (sb-ext:posix-getenv "COOKIE_DOMAIN")))
  (:documentation "An extension of neo4cl:bolt-server, with added slots for user-management."))


;;; Helper functions

(defun make-auth-bolt-server ()
  "Return an instance of `neo4cl:bolt-server` for connecting to the authentication and management instance.
   Default to using the cookie-domain, because the dot-delimiters cause no problem here."
  (make-instance
    'auth-bolt-server
    :hostname (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
    :port (if (sb-ext:posix-getenv "AUTH_NEO4J_PORT")
              (parse-integer (sb-ext:posix-getenv "AUTH_NEO4J_PORT"))
              7687)
    ;; Adapt to the authentication scheme in effect
    :auth-token (cond
                  ;; Basic authentication
                  ;; Not implementing null auth for this, because it'd be stupid.
                  ;; If we're using `docker secrets` with Docker Swarm,
                  ;; we need to pull the password from a file.
                  ((or (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                       (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD"))
                   (make-instance
                     'neo4cl:bolt-auth-basic
                     :username (or (sb-ext:posix-getenv "AUTH_NEO4J_USER")
                                   "neo4j")
                     ;; Again, check for the file _first_.
                     ;; If that's specified, it takes precedence over one supplied via env var.
                     :password (if (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                   (with-open-file (infile (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                                           :direction :input)
                                     (read-line infile))
                                   (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD"))))
                  (t
                   (error "Need both AUTH_NEO4J_USER and either AUTH_NEO4J_PASSWORD or AUTH_NEO4J_PASSWORD_FILE")))))

(defun hash-password (password)
  "Hash a user's password."
  (ironclad:pbkdf2-hash-password-to-combined-string
   (flexi-streams:string-to-octets password)
   :digest 'ironclad:sha512))

(defun validate-password (password hash)
  "Compares a password against the stored hash."
  (ironclad:pbkdf2-check-password (flexi-streams:string-to-octets password) hash))


;;; Methods

(defmethod add-domain ((server auth-bolt-server))
  (log-message
    :info
    (format nil "Ensuring the presence of the domain '~A' on the Neo4j authentication server."
            (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    ;; First check whether the domain is already present.
    (if (neo4cl:bolt-transaction-autocommit
          session
          "MATCH (d:Domain {name: $domain}) RETURN d.ScCreateddate"
          :parameters `(("domain" . ,(domain server))))
      ;; Don't throw an error if it's there.
      ;; The only time it should *not* be present is on initial startup.
      (log-message :debug (format nil "Domain '~A' is already present" (domain server)))
      ;; If not, take action.
      (progn
        ;; First, ensure the necessary uniqueness constraints are present.
        (log-message :debug "Ensuring presence of uniqueness constraints on domains, accounts and sessions.")
        (neo4cl:bolt-transaction-autocommit
          session
          "CREATE CONSTRAINT unique_domain IF NOT EXISTS ON (d:Domain) ASSERT d.name IS UNIQUE")
        (neo4cl:bolt-transaction-autocommit
          session
          "CREATE CONSTRAINT unique_session IF NOT EXISTS ON (s:Session) ASSERT s.name IS UNIQUE")
        ;; Next, register the domain itself.
        (neo4cl:bolt-transaction-autocommit
          session
          "CREATE (:Domain {name: $domain, ScCreateddate: $timestamp})"
          :parameters `(("domain" . ,(domain server))
                        ("timestamp" . ,(get-universal-time))))))
    ;; Clean up
    (neo4cl:disconnect session))
  ;; Explicitly return a positive value
  t)

(defmethod add-account ((server auth-bolt-server)
                        (username string)
                        (password string))
  (log-message :debug (format nil "Attempting to create new account in domain '~A' for username '~A'"
                              (domain server) username))
  (let ((session (neo4cl:establish-bolt-session server)))
    ;; First check whether we already have an account by this name
    (if (neo4cl:bolt-transaction-autocommit
          session
          "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) RETURN a.ScCreateddate"
          :parameters `(("domain" . ,(domain server))
                        ("username" . ,username)))
      (error 'client-error :message
             (format nil "Domain '~A' already contains an account with name '~A'"
                     (domain server) username))
      ;; If not, create the account
      (progn
        (log-message :debug (format nil "Account '~A' does not already exist. Creating..." username))
        (neo4cl:bolt-transaction-autocommit
          session
          "MATCH (d:Domain {name: $domain}) CREATE (d)-[:HAS]->(:Account {name: $username, passwordhash: $passwordhash, ScCreateddate: $timestamp, active: true})"
          :parameters `(("domain" . ,(domain server))
                        ("username" . ,username)
                        ("passwordhash" . ,(hash-password password))
                        ("timestamp" . ,(get-universal-time))))))
    (neo4cl:disconnect session))
  ;; Explicitly return a positive value if we got this far
  t)

(defmethod fetch-accounts ((server auth-bolt-server))
  (log-message :debug (format nil "Fetching account listing for domain '~A'" (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    (let ((result (neo4cl:bolt-transaction-autocommit
                    session
                    "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account) RETURN a.name AS name, a.ScCreateddate AS createddate, a.active AS active"
                    :parameters `(("domain" . ,(domain server))))))
      (neo4cl:disconnect session)
      result)))

(defmethod change-password ((server auth-bolt-server)
                            (username string)
                            (password string))
  (log-message :debug (format nil "Attempting to change password in domain '~A' for user '~A'"
                              (domain server) username))
  (let ((session (neo4cl:establish-bolt-session server)))
    ;; Change the password itself
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) SET a.passwordhash = $passwordhash"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)
                    ("passwordhash" . ,(hash-password password))))
    ;; Now remove all their sessions, to force re-authentication
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(:Account {name: $username})-[:HAS]->(s:Session) DETACH DELETE s"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    (neo4cl:disconnect session))
    ;; Explicitly return a positive value
    t)

(defmethod deactivate-account ((server auth-bolt-server)
                               (username string))
  (log-message :debug (format nil "Attempting to deactivate account '~A' in domain '~A'"
                              username (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    ;; Deactivate the account itself
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) SET a.active = false"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    ;; Now that we've prevented them logging back in, remove all their sessions
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(:Account {name: $username})-[:HAS]->(s:Session) DETACH DELETE s"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    (neo4cl:disconnect session))
  ;; Explicitly return a positive value
  t)

(defmethod reactivate-account ((server auth-bolt-server)
                               (username string))
  (log-message :debug (format nil "Attempting to reactivate account '~A' in domain '~A'"
                              username (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) SET a.active = true"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    (neo4cl:disconnect session))
  ;; Explicitly return a positive value
  t)

(defmethod delete-account ((server auth-bolt-server)
                           (username string))
  (log-message :debug (format nil "Attempting to delete account '~A' in domain '~A'"
                              username (domain server)))
  (let ((session (neo4cl:establish-bolt-session server)))
    ;; Delete the account itself
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) DETACH DELETE a"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    ;; Now that we've prevented them logging back in, remove all their sessions
    (neo4cl:bolt-transaction-autocommit
      session
      "MATCH (:Domain {name: $domain})-[:HAS]->(:Account {name: $username})-[:HAS]->(s:Session) DETACH DELETE s"
      :parameters `(("domain" . ,(domain server))
                    ("username" . ,username)))
    (neo4cl:disconnect session))
  ;; Explicitly return a positive value
  t)

(defmethod check-user-creds ((server auth-bolt-server)
                             (username string)
                             (password string))
  (log-message :debug (format nil "Checking credentials for account '~A'" username))
  (if (equal "ScSystem" username)
    (error "Not permitted to log in as ScSystem")
    (let* ((session (neo4cl:establish-bolt-session server))
           (stored-hash (neo4cl:bolt-transaction-autocommit
                          session
                          "MATCH (:Domain {name: $domain})-[:HAS]->(a:Account {name: $username}) RETURN a.passwordhash AS passwordhash"
                          :parameters `(("domain" . ,(domain server))
                                        ("username" . ,username)))))
      (neo4cl:disconnect session)
      (log-message :debug (format nil "Fetched credential result ~A" stored-hash))
      (when stored-hash (validate-password password
                                           (cdr (assoc "passwordhash" (car stored-hash)
                                                       :test #'equal)))))))
