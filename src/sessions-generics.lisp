;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Classes and generic functions for session management

(in-package :syscat)


(defclass session ()
  ((identifier :initarg :identifier
               :reader identifier
               :type (string 32)  ; The length of a V4 UUID
               :initform (with-output-to-string (str)
                           (uuid:print-bytes str (uuid:make-v4-uuid))
                           str)
               :documentation "The (hopefully unique) identifier token for a session.
                               This should be set explicitly when you fetch a session's details from the store,
                               but left to be auto-generated when creating a session on login.
                               Default is a V4 UUID.")
   (username :initarg :username
             :reader username
             :type string
             :initform (error "username must be specified.")
             :documentation "The user to whom this session belongs.")
   (domain :initarg :domain
              :reader domain
              :type string
              :initform (error "domain must be specified.")
              :documentation "The domain to which the user belongs."))
  (:documentation "All the details pertinent to a session."))


(defgeneric check-session-server (server)
            (:documentation "Health-check on the session server."))

(defgeneric store-session (server session)
            (:documentation "Store a session's details in the server."))

(defgeneric fetch-session (server session-id)
            (:documentation "Fetch the session object matching this identifier.
                            Return a `session` instance on success, or nil on failure."))

(defmethod fetch-session (server session-id)
  "Default implementation"
  (declare (ignore server session-id))
  (log-message :debug "Invalid request for session details. Returning nil.")
  ;; Return the only sensible value we can
  nil)


(defgeneric delete-session (server session-id)
            (:documentation "Delete the specified session."))
